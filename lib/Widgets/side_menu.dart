import 'package:flutter/material.dart';

class NavigationMenu extends StatelessWidget {
  final String title;
  final Widget destination;
  final Function? onReturn;

  const NavigationMenu({
    Key? key,
    required this.title,
    required this.destination,
    this.onReturn,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => destination))
            .then((_) {
          if (onReturn != null) {
            onReturn!();
          }
        });
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),

        child: Center(
          child: Text(
            title,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
