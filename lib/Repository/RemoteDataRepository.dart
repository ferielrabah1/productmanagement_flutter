import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter_crud_app/models/ProductsModified.dart';
import '../models/product.dart';
import 'package:http/http.dart' as http;

class RemoteDataRepository {
  final String apiUrl;
  late Dio _dio;

  RemoteDataRepository({required this.apiUrl}) {
    _dio = Dio(BaseOptions(baseUrl: apiUrl));
  }


  Future<List<Product>> fetchProducts() async {
    try {
      final response = await _dio.get('/products');

      if (response.statusCode == 200) {
        List<dynamic> jsonData = response.data;
        return jsonData.map((json) => Product.fromJson(json)).toList();
      } else {
        throw Exception('Failed to load products');
      }
    } catch (e) {
      throw Exception('Failed to load products: $e');
    }
  }

  Future<void> insertProduct(Product product) async {
    final response = await _dio.post(
      '/products',
      data: product.toJson(),
      options: Options(
        headers: {
          'Content-Type': 'application/json',
        },
      ),
    );
  }

  Future<void> deleteProduct(int id) async {
    try {
      final response = await _dio.delete(
        '/products/$id',
      );

      if (response.statusCode != 200) {
        throw Exception('Failed to delete product');
      }
    } catch (e) {
      throw Exception('Failed to delete product: $e');
    }
  }

  Future<void> updateProduct(Product product) async {
    try {
      print('Sending product update request for ID: ${product.id}');
      print('Product data: ${product.toJson()}');
      final data = {'stock': product.stock};
      final response = await _dio.patch(
        '/products/${product.id}',
        data: data,
      );
    } catch (e) {
      print('Failed to update product: $e');
    }
  }


  Future<Product> fetchProductById(int productId) async {
    try {
      final response = await _dio.get('/products/$productId');
      if (response.statusCode == 200) {
        return Product.fromJson(response.data);
      } else {
        throw Exception('Failed to fetch product');
      }
    } catch (e) {
      throw Exception('Failed to fetch product: $e');
    }
  }

  Future<List<ProductsModified>> fetchProductsToSyncFromRemote() async {
    try {
      final response = await _dio.get('/modified');

      if (response.statusCode == 200) {
        List<dynamic> jsonData = response.data;
        return jsonData.map((json) => ProductsModified.fromMap(json)).toList();
      } else {
        throw Exception('Failed to load products');
      }
    } catch (e) {
      throw Exception('Failed to load products: $e');
    }
  }



  Future<void> updateProductSyncStatus(ProductsModified product) async {
    try {
      print('Product data: ${product.toJson()}');
      final data = {'sync': product.sync};
      final response = await _dio.patch(
        '/modified/sync/${product.id}',
        data: data,
      );
    } catch (e) {
      print('Failed to update product: $e');
    }
  }


  Future<void> deleteOperation(int id) async {
    try {
      final response = await _dio.delete(
        '/modified/delete/$id',
      );

      if (response.statusCode != 200) {
        throw Exception('Failed to delete product');
      }
    } catch (e) {
      throw Exception('Failed to delete product: $e');
    }
  }


}