import '../db/db_helperProduct.dart';
import '../models/product.dart';
import 'DataRepository.dart';

class LocalDataRepository implements DataRepository {
  final ProductHelper productHelper;

  LocalDataRepository(this.productHelper);

  @override
  Future<List<Product>> fetchProducts() async {
    final List<Map<String, dynamic>> productMaps = await productHelper.queryAllProducts();
    return productMaps.map((productMap) => Product.fromMap(productMap)).toList();
  }

  @override
  Future<void> deleteProduct(int id) async {
    await productHelper.deleteProduct(id);
  }

  @override
  Future<void> updateProduct(Product product) async {
    await productHelper.updateProduct(product);
  }

  /*Future<void> updateLocalProducts(List<Product> updatedProducts) async {
    final db = await database;
    Batch batch = db.batch();

    for (var product in updatedProducts) {
      batch.update(
        'products',
        product.toMap(),
        where: 'id = ?',
        whereArgs: [product.id],
      );
    }

    await batch.commit(noResult: true);
  }
*/
}
