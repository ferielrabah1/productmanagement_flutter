import 'package:flutter/foundation.dart';

import '../models/product.dart';

abstract class DataRepository {
  Future<List<Product>> fetchProducts();


}