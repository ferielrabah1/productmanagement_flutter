import '../db/db_helperProduct.dart';
import '../models/product.dart';
import '../services/connectivityService.dart';
import 'RemoteDataRepository.dart';

class DataRepositoryManager {
  final ProductHelper localRepository;
  final RemoteDataRepository remoteRepository;
  final ConnectivityService cnxSrv;

  DataRepositoryManager({
    required this.localRepository,
    required this.remoteRepository,
    required this.cnxSrv,
  });


  Future<void> syncRemoteToLocal() async {
    try {
      List<Product> remoteProducts = await remoteRepository.fetchProducts();
      final List<Map<String, dynamic>> localProductsData = await localRepository.queryAllProducts();
      List<Product> localProducts = localProductsData.map((data) => Product.fromJson(data)).toList();

      Map<String, Product> localProductsMap = {
        for (var product in localProducts) product.name: product
      };

      for (Product remoteProduct in remoteProducts) {
        if (!localProductsMap.containsKey(remoteProduct.name)) {
          await localRepository.insertProduct(remoteProduct);
          print('Added product ${remoteProduct.name} to local');
        }
      }

      // Remove products from local that are deleted remotely
      for (Product localProduct in localProducts) {
        bool foundInRemote = remoteProducts.any((remoteProduct) => remoteProduct.name == localProduct.name);
        if (!foundInRemote) {
          await localRepository.deleteProduct(localProduct.id!);
          print('Deleted product ${localProduct.name} from local');
        }
      }

      //Update


      print("Synced from remote to local");
    } catch (e) {
      print('Error syncing from remote to local: $e');
    }
  }


  Future<List<Product>> fetchProducts() async {
    try {
      if (cnxSrv.isConnected) {
        return await remoteRepository.fetchProducts();
      } else {
        final maps = await localRepository.queryAllProducts();
        return maps.map((map) => Product.fromMap(map)).toList();
      }
    } catch (e) {
      print('Failed to fetch products: $e');
      final maps = await localRepository.queryAllProducts();
      return maps.map((map) => Product.fromMap(map)).toList();
    }
  }




  Future<void> deleteProduct(int id) async {
    if (cnxSrv.isConnected) {
     // await remoteRepository.deleteProduct(id);
      await localRepository.deleteProduct(id);
     // await synchronizeData();
      print("Deleted from both remote and local repositories");
    } else {
      await localRepository.deleteProduct(id);
      print('Deleted from local repository only.');
    }
  }

  Future<void> updateProduct(Product product) async {
      //await remoteRepository.updateProduct(product);
      await localRepository.updateProduct(product);
    }
  }

/*  Future<void> addProduct(Product product) async {
    try {
      await localRepository.insertProduct(product);
      if (cnxSrv.isConnected) {
        await remoteRepository.insertProduct(product);
      }
    } catch (e) {
      print('Failed to add product: $e');
    }
  }*/

