import 'dart:math';
import 'package:barcode_widget/barcode_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_crud_app/db/db_CategorieHelper.dart';
import 'package:flutter_crud_app/db/db_helperProduct.dart';
import 'package:flutter_crud_app/models/categorie.dart';
import 'package:flutter_crud_app/models/product.dart';
import 'package:flutter_crud_app/screens/Product/product_list_screen.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:flutter_crud_app/screens/Categorie/category_management_screen.dart';
import 'package:flutter_crud_app/screens/Sales/sales_management.dart';
import 'package:easy_sidemenu/easy_sidemenu.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';

import '../Sales/orders_list_screen.dart';

class AddProductScreen extends StatefulWidget {
  @override
  _AddProductScreenState createState() => _AddProductScreenState();
}

class _AddProductScreenState extends State<AddProductScreen> {
  final _formKey = GlobalKey<FormState>();
  final SideMenuController _sideMenuController = SideMenuController();
  final TextEditingController _barCodeController = TextEditingController();
  final TextEditingController _refController = TextEditingController();

  late String _name;
  late String _description;
  late double _price;
  late int _stock;
  late int _ref;
  File? _imageFile;
  Categorie? _selectedCategorie;
  List<Categorie> _categories = [];
  late String _barCode;

  final ProductHelper productHelper = ProductHelper();
  final CategorieHelper categorieHelper = CategorieHelper();

  @override
  void initState() {
    super.initState();
    _loadCategories();
  }

  Future<void> _loadCategories() async {
    _categories = await categorieHelper.queryAllCategories();
    if (_categories.isNotEmpty) {
      setState(() {
        _selectedCategorie = _categories[0];
      });
    }
  }

  Future<void> _pickImage() async {
    final pickedFile = await ImagePicker().pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
      });
    }
  }

  void _saveProduct() async {
    if (_formKey.currentState!.validate() && _imageFile != null) {
      _formKey.currentState!.save();
      bool productExists = await productHelper.isProductExists(_name);
      bool codeExists = await productHelper.isCodeExists(_barCode);
      bool refExists = await productHelper.isRefExists(_ref);

      if(refExists){
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content : Text('Un produit avec cette référence existe déja'),
        ),
        );
      }
      if (productExists) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Un produit avec ce nom existe déjà'),
          ),
        );
        return;
      }
      if (codeExists) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Un produit avec ce code existe déjà'),
          ),
        );
        return;
      }

      if (_selectedCategorie == null) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Veuillez sélectionner une catégorie'),
          ),
        );
        return;
      }

      final product = Product(
        name: _name,
        description: _description,
        price: _price,
        imageUrl: _imageFile!.path,
        categorieId: _selectedCategorie?.id ?? 0,
        stock: _stock,
        barCode: _barCode,
        ref: _ref,
      );

      await productHelper.insertProduct(product);
      Navigator.of(context).pop();
      print(product.categorieId);
    } else if (_imageFile == null) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Veuillez choisir une image')),
      );
    }
  }

  void _generateBarcode() async {
    if (_refController.text.isEmpty || int.tryParse(_refController.text) == null) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Veuillez entrer une référence valide')),
      );
      return;
    }

    setState(() {
      _ref = int.parse(_refController.text);
    });

    String barcodeData = '619$_ref';
    setState(() {
      _barCodeController.text = barcodeData;
    });

    final pdf = await generateBarcodePdf(barcodeData);

    final tempDir = await getTemporaryDirectory();
    final pdfFile = File('${tempDir.path}/barcode.pdf');
    await pdfFile.writeAsBytes(await pdf.save());

    try {
      await OpenFile.open(pdfFile.path);
    } catch (e) {
      print('Error opening PDF: $e');
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Failed to open PDF'),
        ),
      );
      return;
    }

    await Printing.layoutPdf(
      onLayout: (PdfPageFormat format) async => pdf.save(),
    );

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text('Barcode printed successfully!'),
      ),
    );
  }

  Future<pw.Document> generateBarcodePdf(String barcodeData) async {
    final pdf = pw.Document();
    pdf.addPage(
      pw.Page(
        build: (pw.Context context) {
          return pw.Center(
            child: pw.BarcodeWidget(
              barcode: pw.Barcode.code128(),
              data: barcodeData,
              width: 200,
              height: 80,
            ),
          );
        },
      ),
    );
    return pdf;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Row(
          children: [
          Container(
          width: 250,
          child: SideMenu(
            controller: _sideMenuController,
            style: SideMenuStyle(
              backgroundColor: Colors.deepPurple,
              displayMode: SideMenuDisplayMode.auto,
              hoverColor: Colors.white10,
              selectedTitleTextStyle: TextStyle(color: Colors.white),
              unselectedTitleTextStyle: TextStyle(color: Colors.white70),
              selectedIconColor: Colors.white,
              unselectedIconColor: Colors.white70,
              itemOuterPadding: EdgeInsets.symmetric(vertical: 8.0),
            ),
            title: Container(
              height: 180.0,
              child: Align(
                alignment: Alignment.bottomCenter,
              ),
            ),
            items: [
              SideMenuItem(
                title: 'Ajouter produit',
                onTap: (index, controller) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AddProductScreen(),
                    ),
                  );
                },
                icon: Icon(Icons.add, color: Colors.white),
              ),
              SideMenuItem(
                title: 'Categories',
                onTap: (index, controller) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CategoryManagementScreen(),
                    ),
                  );
                },
                icon: Icon(Icons.category, color: Colors.white),
              ),
              SideMenuItem(
                title: 'Produits',
                onTap: (index, controller) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ProductListScreen(),
                      ));
                },
                icon: Icon(Icons.list, color: Colors.white),
              ),
              SideMenuItem(
                title: 'Point de vente',
                onTap: (index, controller) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SalesManagementScreen(),
                    ),
                  );
                },
                icon: Icon(Icons.shopping_cart_checkout, color: Colors.white),
              ),
              SideMenuItem(
                title: 'Ventes',
                onTap: (index, controller) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => OrderListScreen(),
                    ),
                  );
                },
                icon: Icon(Icons.sell, color: Colors.white),
              ),
              SideMenuItem(
                title: 'Exit',
                icon: Icon(Icons.exit_to_app, color: Colors.white),
              ),
            ],
          ),
        ),
        Expanded(
        child: Padding(
        padding: const EdgeInsets.all(16.0),
    child: Center(
    child: Form(
    key: _formKey,
    child: SingleChildScrollView(
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
    Container(
    width: 400,
    child: TextFormField(
    decoration: InputDecoration(
    hintText: 'Nom du produit',
    hintStyle: TextStyle(color: Colors.grey),
    filled: true,
    fillColor: Colors.white,
    border: OutlineInputBorder(),
    ),
    validator: (value) {
    if (value == null || value.isEmpty) {
    return 'Veuillez entrer le nom du produit';
    }
    return null;
    },
    onSaved: (value) {
    _name = value!;
    },
    ),
    ),
    SizedBox(height: 16.0),
    Container(
    width: 400,
    child:
    TextFormField(

      decoration: InputDecoration(
        hintText: 'Stock',
        hintStyle: TextStyle(color: Colors.grey),
        filled: true,
        fillColor: Colors.white,
        border: OutlineInputBorder(),
      ),
      keyboardType: TextInputType.number,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Veuillez entrer la quantité disponible du produit';
        }
        if (int.tryParse(value) == null) {
          return 'La valeur du stock doit être numérique';
        }
        return null;
      },
      onSaved: (value) {
        _stock = int.parse(value!);
      },
    ),
    ),
      SizedBox(height: 16.0),
Container(
  width: 400,
  child : TextFormField(
    decoration: InputDecoration(
      hintText: 'Référence',
      hintStyle: TextStyle(color: Colors.grey),
      filled: true,
      fillColor: Colors.white,
      border: OutlineInputBorder(),
    ),
    keyboardType: TextInputType.number,
    controller: _refController,
    validator: (value) {
      if (value == null || value.isEmpty) {
        return 'Veuillez entrer la référence du produit';
      }
      if (int.tryParse(value) == null) {
        return 'La référence doit être numérique';
      }
      return null;
    },
    onSaved: (value) {
      _ref = int.parse(value!);
    },
  ),

),
      SizedBox(height: 16.0),

      Container(
        width: 400,
        child: TextFormField(
          decoration: InputDecoration(
            hintText: 'Description',
            hintStyle: TextStyle(color: Colors.grey),
            filled: true,
            fillColor: Colors.white,
            border: OutlineInputBorder(),
          ),
          validator: (value) {
            if (value == null || value.isEmpty) {
              return 'Veuillez entrer la description du produit';
            }
            return null;
          },
          onSaved: (value) {
            _description = value!;
          },
        ),
      ),
      SizedBox(height: 16.0),
Container(
  width: 400,
  child :       TextFormField(
    keyboardType: TextInputType.number,
    decoration: InputDecoration(
      hintText: 'Prix',
      hintStyle: TextStyle(color: Colors.grey),
      filled: true,
      fillColor: Colors.white,
      border: OutlineInputBorder(),
    ),
    validator: (value) {
      RegExp redExp = RegExp('[0-9]');
      if (value == null || value.isEmpty) {
        return 'Veuillez entrer le prix du produit';
      }
      if (!redExp.hasMatch(value)) {
        return 'La valeur du prix doit être numérique';
      }
      return null;
    },
    onSaved: (value) {
      _price = double.parse(value!);
    },
  ),

),
      SizedBox(height: 16.0),
Container(
  width: 400,
  child :      Row(
    children: [
      Expanded(

        flex: 3,
        child: TextFormField(
          controller: _barCodeController,
          //maxLength: 13,
          decoration: InputDecoration(
            hintText: 'Code à barre',
            hintStyle: TextStyle(color: Colors.grey),
            filled: true,
            fillColor: Colors.white,
            border: OutlineInputBorder(),
            counterText: '',
          ),
          validator: (value) {
            RegExp regExp = RegExp(r'^[0-9]');
            if (value == null || value.isEmpty) {
              return 'Veuillez entrer le code à barre du produit';
            }
            if (!regExp.hasMatch(value)) {
              return 'Le code à barres doit etre numérique';
            }
            return null;
          },
          onSaved: (value) {
            _barCode = value!;
          },
        ),
      ),
      SizedBox(width: 10),
      Flexible(
        flex: 1,
        child: IconButton(
          icon: Icon(Icons.code, color: Colors.grey),
          onPressed: _generateBarcode,
        ),
      ),
    ],
  ),

),
      SizedBox(height: 16),

      Container(
        width: 400,
        child: DropdownButtonFormField<Categorie>(
          value: _selectedCategorie,
          decoration: InputDecoration(
            hintText: 'Sélectionner une catégorie',
            hintStyle: TextStyle(color: Colors.grey),
            filled: true,
            fillColor: Colors.white,
            border: OutlineInputBorder(),
          ),
          items: _categories.map((Categorie categorie) {
            return DropdownMenuItem<Categorie>(
              value: categorie,
              child: Text(categorie.nom),
            );
          }).toList(),
          onChanged: (Categorie? newValue) {
            setState(() {
              _selectedCategorie = newValue!;
            });
          },
          validator: (value) {
            if (value == null) {
              return 'Veuillez sélectionner une catégorie';
            }
            return null;
          },
        ),
      ),
      SizedBox(height: 16.0),

      ElevatedButton(
        onPressed: _pickImage,
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.blueAccent,
        ),
        child: Text(
          'Sélectionner une image',
          style: TextStyle(color: Colors.white),
        ),
      ),
      SizedBox(height: 16.0),

      _imageFile != null
          ? Image.file(
        _imageFile!,
        height: 150,
      )
          : Container(),

      SizedBox(height: 16.0),

      ElevatedButton(
        onPressed: _saveProduct,
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.blueAccent,
        ),
        child: Text(
          'Ajouter produit',
          style: TextStyle(color: Colors.white),
        ),
      ),
    ],
    ),
    ),
    ),
    ),
        ),
        ),
          ],
        ),
      backgroundColor: Colors.white,
    );
  }
}
