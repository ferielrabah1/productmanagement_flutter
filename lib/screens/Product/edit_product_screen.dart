import 'package:flutter/material.dart';
import 'package:flutter_crud_app/Repository/RemoteDataRepository.dart';
import 'package:flutter_crud_app/db/db_helperProduct.dart';
import 'package:flutter_crud_app/db/db_CategorieHelper.dart';
import 'package:flutter_crud_app/models/product.dart';
import 'package:flutter_crud_app/models/categorie.dart';
import 'package:flutter_crud_app/services/connectivityService.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import '../../Repository/DataRepositoryManager.dart';

class EditProductScreen extends StatefulWidget {
  final Product product;

  EditProductScreen({
    required this.product,
  });

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  final _formKey = GlobalKey<FormState>();
  late String _name;
  late String _description;
  late double _price;
  late String _imageUrl;
  late String _category;
  File? _imageFile;
  late int _stock;
  late String _barCode;
  late int? _ref;

  final ProductHelper productHelper = ProductHelper();
  final CategorieHelper categorieHelper = CategorieHelper();
  late List<Categorie> _categories = [];


  @override
  void initState() {
    super.initState();
    _name = widget.product.name;
    _description = widget.product.description;
    _stock = widget.product.stock;
    _ref = widget.product.ref;
    _price = widget.product.price;
    _imageUrl = widget.product.imageUrl;
    _category = widget.product.categorieId.toString();
    _barCode = widget.product.barCode;
    _loadCategories();
  }

  Future<void> _loadCategories() async {
    final categoriesData = await categorieHelper.queryAllCategories();
    setState(() {
      _categories = categoriesData;
    });
  }

  Future<void> _pickImage() async {
    final pickedFile =
    await ImagePicker().pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        _imageFile = File(pickedFile.path);
        _imageUrl = pickedFile.path;
      });
    }
  }

  void _updateProduct() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      final updatedProduct = Product(
        id: widget.product.id,
        name: _name,
        description: _description,
        price: _price,
        imageUrl: _imageUrl,
        categorieId: int.parse(_category),
        stock: _stock,
        barCode: _barCode,
        ref: _ref,
      );


        productHelper.updateProduct(updatedProduct);

    } else if (_imageFile == null) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Veuillez choisir une image')),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Modification d\'un produit'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                TextFormField(
                  initialValue: _name,
                  decoration: InputDecoration(
                    hintText: 'Nom du produit',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Veuillez entrer le nom du produit';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _name = value!;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  initialValue: _stock.toString(),
                  decoration: InputDecoration(
                    hintText: 'Stock',
                  ),
                  keyboardType: TextInputType.number,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Veuillez entrer la quantité disponible du produit';
                    }
                    if (int.tryParse(value) == null) {
                      return 'La valeur du stock doit être numérique';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _stock = int.parse(value!);
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  initialValue: _ref.toString(),
                  decoration: InputDecoration(
                    hintText: 'Ref',
                  ),
                  keyboardType: TextInputType.number,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Veuillez entrer la référence du produit';
                    }
                    if (int.tryParse(value) == null) {
                      return 'La référence doit être numérique';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _ref = int.parse(value!);
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  initialValue: _description,
                  decoration: InputDecoration(
                    hintText: 'Description',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Veuillez entrer la description du produit ';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _description = value!;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  initialValue: _price.toString(),
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: 'Prix',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Veuillez enter le prix du produit';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _price = double.parse(value!);
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  initialValue: _barCode,
                  keyboardType: TextInputType.number,
                  maxLength: 13,
                  decoration: InputDecoration(
                    hintText: 'Code à barre',
                  ),
                  validator: (value) {
                    RegExp regExp = RegExp(r'^[0-9]');
                    if (value == null || value.isEmpty) {
                      return 'Veuillez entrer le code à barre du produit';
                    }
                    if (!regExp.hasMatch(value)) {
                      return 'Le code à barres doit etre numérique';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _barCode = value!;
                  },
                ),
                SizedBox(height: 16.0),
                DropdownButtonFormField<String>(
                  value: _category,
                  decoration: InputDecoration(
                    hintText: 'Catégorie',
                  ),
                  items: _categories.map((category) {
                    return DropdownMenuItem<String>(
                      value: category.id.toString(),
                      child: Text(category.nom),
                    );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      _category = value!;
                    });
                  },
                ),
                SizedBox(height: 16.0),
                ElevatedButton(
                  onPressed: _pickImage,
                  child: Text('Sélectionner une image'),
                ),
                SizedBox(height: 16.0),
                _imageFile != null
                    ? Image.file(
                  _imageFile!,
                  height: 150,
                )
                    : Container(),
                SizedBox(height: 16.0),
                ElevatedButton(
                  onPressed: _updateProduct,
                  child: Text('Modifier produit'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
