import 'package:flutter/material.dart';
import 'package:flutter_crud_app/db/db_helperProduct.dart';
import 'package:flutter_crud_app/db/db_CategorieHelper.dart';
import 'package:flutter_crud_app/models/product.dart';
import 'package:flutter_crud_app/screens/Product/add_product_screen.dart';
import 'package:flutter_crud_app/screens/Product/edit_product_screen.dart';
import 'package:flutter_crud_app/screens/Categorie/category_management_screen.dart';
import 'package:flutter_crud_app/screens/Sales/sales_management.dart';
import 'package:flutter_crud_app/models/categorie.dart';
import 'dart:io';
import 'package:easy_sidemenu/easy_sidemenu.dart';

import 'package:flutter_crud_app/Repository/DataRepositoryImpl.dart';
import 'package:flutter_crud_app/Repository/DataRepositoryManager.dart';
import 'package:flutter_crud_app/Repository/RemoteDataRepository.dart';

import 'package:flutter_crud_app/screens/Sales/orders_list_screen.dart';
import 'package:flutter_crud_app/services/connectivityService.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';

class ProductListScreen extends StatefulWidget {
  final int? categoryIdFilter;
  ProductListScreen({Key? key, this.categoryIdFilter}) : super(key: key);

  @override
  _ProductListScreenState createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen> {
  final SideMenuController _sideMenuController = SideMenuController();
  final RemoteDataRepository remoteRepositoryManager = RemoteDataRepository(apiUrl: 'http://localhost:7001/api');

  late DataRepositoryManager repositoryManager;
  late List<Product> _products = [];
  late List<Product> _filteredProducts = [];
  late List<Categorie> _categories = [];
  late List<String> _categoryNames = [];

  final ProductHelper productHelper = ProductHelper();
  final CategorieHelper categorieHelper = CategorieHelper();
  final TextEditingController _searchController = TextEditingController();
  String _searchQuery = '';

  int _currentPage = 1;
  final int _productsPerPage = 9;
  Categorie? _selectedCategorie;
  String? _selectedSortOption;
  bool _isLoading = false;

 /* late Product product = Product(
    name: '',
    description: '',
    price: 0.0,
    imageUrl: '',
    categorieId: 0,
    ref: 0,
    stock: 0,
    barCode: '',
  );*/
  final ConnectivityService connectivityService = ConnectivityService();
  bool _isConnectedMessageDisplayed = false;
  bool _isDisconnectedMessageDisplayed = false;
  @override
  @override
  void initState() {
    super.initState();
    connectivityService.initialize();

    _searchController.addListener(_onSearchChanged);

    final localRepository = LocalDataRepository(productHelper);
    final remoteRepository = RemoteDataRepository(apiUrl: 'http://localhost:7001/api');
    repositoryManager = DataRepositoryManager(
      localRepository: productHelper,
      remoteRepository: remoteRepository,
      cnxSrv: connectivityService,
    );

    _loadCategories();
    _loadProducts();

   /* connectivityService.subscription.onData((InternetStatus status) {
      if (status == InternetStatus.connected) {
        if (!_isConnectedMessageDisplayed) {
          _isConnectedMessageDisplayed = true;
          _isDisconnectedMessageDisplayed = false;
          _switchToRemoteWithDialog('Synchronisation de données réussie.', context);



        }
      } else {
        if (!_isDisconnectedMessageDisplayed) {
          _isDisconnectedMessageDisplayed = true;
          _isConnectedMessageDisplayed = false;
          _switchToLocalWithDialog(
              'Utilisation des données locales. Synchronisation en attente de connexion réseau.', context);
          setState(() {
            _isLoading = false;
          });

        }
      }
    });*/
  }

  @override
  void dispose() {
    _searchController.removeListener(_onSearchChanged);
    _searchController.dispose();
   connectivityService.dispose();

    super.dispose();
  }


  void _onSearchChanged() {
    setState(() {
      _searchQuery = _searchController.text.toLowerCase();
      _filteredProducts = _products
          .where((product) => product.name.toLowerCase().contains(_searchQuery))
          .toList();
      _currentPage = 1;
    });
  }
  Future<void> _loadProducts() async {



      List<Product> products;

        final List<Map<String, dynamic>> productsData = await repositoryManager.localRepository.queryAllProducts();
        products = productsData.map((data) => Product.fromJson(data)).toList();


      _products = products.where((product) => !product.deleted).toList();

      if (widget.categoryIdFilter != null) {
        _filteredProducts = _products.where((product) => product.categorieId == widget.categoryIdFilter).toList();
      } else {
        _filteredProducts = _products;
      }

      _onSearchChanged();

  }

  /*Future<void> _loadProducts() async {
    setState(() {
      _isLoading = true;
    });

    try {
      List<Product> products;
      if (!connectivityService.isConnected) {
        final List<Map<String, dynamic>> productsData = await repositoryManager.localRepository.queryAllProducts();
        products = productsData.map((data) => Product.fromJson(data)).toList();
      } else {
        products = await repositoryManager.remoteRepository.fetchProducts();
      }

      _products = products.where((product) => !product.deleted).toList();

      if (widget.categoryIdFilter != null) {
        _filteredProducts = _products.where((product) => product.categorieId == widget.categoryIdFilter).toList();
      } else {
        _filteredProducts = _products;
      }

      _onSearchChanged();
    } catch (e) {
      print('Error loading products: $e');
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }*/

  void _switchToLocalWithDialog(String message , BuildContext context) {
    if (!connectivityService.isConnected) {

      showDialog(context: context,
          builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Passage en mode hors ligne"),
          content: Text(message),
          actions: <Widget>[
        TextButton(
          onPressed: () { Navigator.of(context).pop();
            },
          child: Text("ok"),
        ),



          ],
        );
          });
    }
  }

  void _switchToRemoteWithDialog(String message, BuildContext context) {
    if (connectivityService.isConnected) {

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Confirmation de connexion"),
            content: Text(message),
            actions: <Widget>[
              TextButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop(); // Close the dialog
                },
              ),
            ],
          );
        },
      );
    }
  }


  Future<void> _loadCategories() async {
    final categoriesData = await categorieHelper.queryAllCategories();
    setState(() {
      _categories = categoriesData;
      _categoryNames = _categories.map((category) => category.nom).toList();
      _categoryNames.insert(0, '');
    });
  }

  void _deleteProduct(int productId) async {
    setState(() {
      _isLoading = true;
    });

    try {
      if (await connectivityService.isConnected) {
        // Delete from remote server
        await repositoryManager.remoteRepository.deleteProduct(productId);
        // Also delete from local database
        await repositoryManager.localRepository.deleteProduct(productId);
        print("deleteddddd from both");
      } else {
        // Delete locally and mark for remote deletion
        await repositoryManager.localRepository.deleteProduct(productId);
      }

      // Reload products
      await _loadProducts();
    } catch (e) {
      print('Error deleting product: $e');
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }


  List<Product> _getPaginatedProducts() {
    final startIndex = (_currentPage - 1) * _productsPerPage;
    final endIndex = startIndex + _productsPerPage;
    return _filteredProducts.sublist(
      startIndex,
      endIndex > _filteredProducts.length ? _filteredProducts.length : endIndex,
    );
  }

  List<Product> _getFilteredProducts() {
    List<Product> filteredProducts = [];
    if (_selectedCategorie != null) {
      filteredProducts =
          _products.where((product) => product.categorieId == _selectedCategorie!.id).toList();
    } else {
      filteredProducts = _products.toList();
    }

    if (_selectedSortOption == 'prix décroissant') {
      filteredProducts.sort((a, b) => b.price.compareTo(a.price));
    } else if (_selectedSortOption == 'prix croissant') {
      filteredProducts.sort((b, a) => b.price.compareTo(a.price));
    }

    return filteredProducts;
  }

  void _goToPreviousPage() {
    if (_currentPage > 1) {
      setState(() {
        _currentPage--;
      });
    }
  }

  void _goToNextPage() {
    if (_currentPage * _productsPerPage < _filteredProducts.length) {
      setState(() {
        _currentPage++;
      });
    }
  }

  Future<String> _getCategorieName(int id) async {
    final categorie = await categorieHelper.getCategorieById(id);
    return categorie?.nom ?? '';
  }



  /*Future<void> _loadRemoteProducts() async {
    List<Product> remoteProducts = await repositoryManager.remoteRepository.fetchProducts();
    setState(() {
      _products = remoteProducts.where((product) => !product.deleted).toList();
      if (widget.categoryIdFilter != null) {
        _filteredProducts = _products
            .where((product) => product.categorieId == widget.categoryIdFilter)
            .toList();
      } else {
        _filteredProducts = _products;
      }
    });

    _onSearchChanged();
  }
*/
  @override
  Widget build(BuildContext context) {
    final paginatedProducts = _getPaginatedProducts();


    return Scaffold(
      backgroundColor: Colors.white,


      body: _isLoading ? Center(child : CircularProgressIndicator())

      : Row(

        children: [

          Container(
            width: 250,
            child: Center(
              child: SideMenu(
                controller: _sideMenuController,
                style: SideMenuStyle(
                  backgroundColor: Colors.deepPurple,
                  displayMode: SideMenuDisplayMode.auto,
                  hoverColor: Colors.white10,
                  selectedTitleTextStyle: TextStyle(color: Colors.white),
                  unselectedTitleTextStyle: TextStyle(color: Colors.white70),
                  selectedIconColor: Colors.white,
                  unselectedIconColor: Colors.white70,
                  itemOuterPadding: EdgeInsets.symmetric(vertical: 8.0),
                ),
                title: Container(
                  height: 180.0,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                  ),
                ),
                items: [
                  SideMenuItem(
                    title: 'Ajouter produit',
                    onTap: (index, controller) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AddProductScreen(),
                        ),
                      ).then((value) => _loadProducts());
                    },
                    icon: Icon(Icons.add, color: Colors.white),
                  ),
                  SideMenuItem(
                    title: 'Categories',
                    onTap: (index, controller) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CategoryManagementScreen(),
                        ),
                      );
                    },
                    icon: Icon(Icons.category, color: Colors.white),
                  ),
                  SideMenuItem(
                    title: 'Produits',
                    onTap: (index, controller) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ProductListScreen(),
                        ),
                      ).then((value) => _loadProducts());
                    },
                    icon: Icon(Icons.list, color: Colors.white),
                  ),
                  SideMenuItem(
                    title: 'Point de vente',
                    onTap: (index, controller) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SalesManagementScreen(),
                        ),
                      ).then((value) => _loadProducts());
                    },
                    icon: Icon(Icons.shopping_cart_checkout, color: Colors.white),
                  ),
                  SideMenuItem(
                    title: 'Ventes',
                    onTap: (index, controller) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => OrderListScreen(),
                        ),
                      );
                    },
                    icon: Icon(Icons.sell, color: Colors.white),
                  ),
                  SideMenuItem(
                    title: 'Exit',
                    icon: Icon(Icons.exit_to_app, color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: TextField(
                    controller: _searchController,
                    decoration: InputDecoration(
                      hintText: 'Search products...',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      prefixIcon: Icon(Icons.search),
                      filled: true,
                      fillColor: Colors.white,
                    ),
                  ),
                ),

                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 10.0),
                      child: Text('Filter par : '),
                    ),
                    SizedBox(
                      width: 300.0,
                      child: DropdownButtonFormField<Categorie>(
                        value: _selectedCategorie,
                        decoration: InputDecoration(
                          hintText: 'Filter par catégorie',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          contentPadding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
                          filled: true,
                          fillColor: Colors.white,
                        ),
                        items: _categories.map((Categorie categorie) {
                          return DropdownMenuItem<Categorie>(
                            value: categorie,
                            child: Text(categorie.nom),
                          );
                        }).toList(),
                        onChanged: (Categorie? newValue) {
                          setState(() {
                            _selectedCategorie = newValue;
                            _filteredProducts = _getFilteredProducts();
                          });
                        },
                      ),
                    ),
                    SizedBox(
                      width: 300.0,
                      child: DropdownButtonFormField<String>(
                        value: _selectedSortOption,
                        decoration: InputDecoration(
                          hintText: 'Filter par prix',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          contentPadding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
                          filled: true,
                          fillColor: Colors.white,
                        ),
                        items: [
                          DropdownMenuItem<String>(
                            value: 'prix croissant',
                            child: Text('Prix Croissant'),
                          ),
                          DropdownMenuItem<String>(
                            value: 'prix décroissant',
                            child: Text('Prix Décroissant'),
                          ),
                        ],
                        onChanged: (String? newValue) {
                          setState(() {
                            _selectedSortOption = newValue;
                            _filteredProducts = _getFilteredProducts();
                          });
                        },
                      ),
                    ),
                    SizedBox(width: 20),
                   /*InkWell(
                      onTap: _synchronizeData,
                      child: Icon(
                        Icons.wifi,
                        size: 36.0,
                        color: Colors.blue,
                      ),
                    ),*/
                  ],
                ),
              /*  Row(
                  children: [
                    ElevatedButton(
                      onPressed: _switchToLocal,
                      child: Text('Local Data'),
                    ),
                    SizedBox(width: 10),
                    ElevatedButton(
                      onPressed: _switchToRemote,
                      child: Text('Remote Data'),
                    ),
                  ],
                ),*/
                Expanded(
                  child: paginatedProducts.isEmpty
                      ? Center(
                    child: Text('No products found.'),


                  )

                      : SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Card(
                      margin: EdgeInsets.all(10),
                      elevation: 5,
                      child: DataTable(
                        sortAscending: true,
                        columnSpacing: 25.0,
                        dataRowMaxHeight: double.infinity,
                        dataRowMinHeight: 100,
                        dividerThickness: 1,
                        columns: const [
                          DataColumn(label: Text('Name')),
                          DataColumn(label: Text('Description')),
                          DataColumn(label: Text('Stock')),
                          DataColumn(label: Text('Price')),
                          DataColumn(label: Text('Category')),
                          DataColumn(label: Text('Image')),
                          DataColumn(label: Text('Actions')),
                        ],
                        rows: paginatedProducts.map((product) {
                          return DataRow(cells: [
                            DataCell(Text(product.name)),
                            DataCell(Text(product.description)),
                            DataCell(Text(product.stock.toString())),
                            DataCell(Text('${product.price.toStringAsFixed(3)} DT')),
                            DataCell(FutureBuilder<String>(
                              future: _getCategorieName(product.categorieId),
                              builder: (context, snapshot) {
                                return Text(snapshot.data ?? 'Loading...');
                              },
                            )),
                            DataCell(product.imageUrl.isNotEmpty
                                ? Image.file(
                              File(product.imageUrl),
                              width: 50,
                              height: 50,
                            )
                                : Icon(Icons.image, color: Colors.grey)),
                            DataCell(Row(
                              children: [
                                IconButton(
                                  icon: Icon(Icons.edit, color: Colors.green),
                                  onPressed: () {
                                    Navigator.of(context)
                                        .push(
                                      MaterialPageRoute(
                                        builder: (context) => EditProductScreen(
                                          product: product,
                                        ),
                                      ),
                                    )
                                        .then((_) => _loadProducts());
                                  },
                                ),
                                IconButton(
                                  icon: Icon(Icons.delete, color: Colors.redAccent),
                                  onPressed: () => _deleteProduct(product.id!),
                                ),
                              ],
                            )),
                          ]);
                        }).toList(),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        icon: Icon(Icons.arrow_back),
                        onPressed: _goToPreviousPage,
                      ),
                      SizedBox(width: 20),
                      Text('Page $_currentPage'),
                      SizedBox(width: 20),
                      IconButton(
                        icon: Icon(Icons.arrow_forward),
                        onPressed: _goToNextPage,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
