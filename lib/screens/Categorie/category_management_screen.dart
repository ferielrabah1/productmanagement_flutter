import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_crud_app/db/db_CategorieHelper.dart';
import 'package:flutter_crud_app/models/categorie.dart';
import 'package:flutter_crud_app/screens/Product/add_product_screen.dart';
import 'package:flutter_crud_app/screens/Product/product_list_screen.dart';
import 'package:easy_sidemenu/easy_sidemenu.dart';

import '../../Widgets/side_menu.dart';
import '../../db/db_helperProduct.dart';
import '../../models/product.dart';
import '../Sales/orders_list_screen.dart';
import '../Sales/sales_management.dart';

class CategoryManagementScreen extends StatefulWidget {
  @override
  _CategoryManagementState createState() => _CategoryManagementState();
}

class _CategoryManagementState extends State<CategoryManagementScreen> {
  final _formKey = GlobalKey<FormState>();
  final SideMenuController _sideMenuController = SideMenuController();

  String _nom = '';
  int? _editingCategoryId;
  final CategorieHelper helperCategorie = CategorieHelper();
  final ProductHelper helperProduct = ProductHelper();
  List<Categorie> _categories = [];
  late List<Product> _products;


  @override
  void initState() {
    super.initState();
    _loadCategories();
  }

  Future<void> _loadCategories() async {
    final categoriesData = await helperCategorie.queryAllCategories();
    setState(() {
      _categories = categoriesData;
    });
  }

  Future<void> _saveCategory() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      bool categoryExists = await helperCategorie.isCategoryExists(_nom);
      if (categoryExists) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Cette catégorie existe déjà'),
          ),
        );
        return;
      }

      if (_editingCategoryId == null) {
        final categorie = Categorie(nom: _nom);
        await helperCategorie.insertCategorie(categorie.toMap());
      } else {
        final categorie = Categorie(id: _editingCategoryId, nom: _nom);
        await helperCategorie.updateCategorie(categorie.toMap());
      }

      //_clearForm();
      _loadCategories();
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(_editingCategoryId == null ? 'Catégorie ajoutée avec succès' : 'Catégorie mise à jour avec succès'),
        ),
      );
    }
  }

  Future<void> _deleteProduct(int id) async {
    await helperProduct.deleteProduct(id);
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text('Product deleted'),
      ),
    );

  }
  Future<void> _deleteCategory(int id) async {
    Categorie? category = await helperCategorie.getCategorieById(id);


    // Get products associated with this category
    List<Product> products = await _getProductsByCategoryId(id);

    int? categoryIdFilter = await showDialog<int>(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Supprimer la catégorie "${category?.nom}" ?'),
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text('Vous devez supprimer tous les produits associés à cette catégorie avant.'),
            SizedBox(height: 10),
            Text('Êtes-vous sûr de vouloir continuer ?'),
            SizedBox(height: 10),
          ],
        ),
        actions: <Widget>[
          TextButton(
            child: Text('Annuler'),
            onPressed: () {
              Navigator.of(context).pop(null); // Return null on cancel
            },
          ),
          TextButton(
            child: Text('Voir les produits associés'),
            onPressed: () {
              Navigator.of(context).pop(id); // Pass the category ID here
            },

          ),
          TextButton(
            child: Text('Oui, supprimer la catégorie'),
            onPressed: () async {
              // Supprimer tous les produits associés
              for (var product in products) {
                await _deleteProduct(product.id!);
              }

              // Supprimer la catégorie
              await helperCategorie.deleteCategorie(id);

              // Close dialog after deletion
              Navigator.of(context).pop(null);
            },
          ),
        ],
      ),
    );

    // Navigate to ProductListScreen with categoryIdFilter if chosen
    if (categoryIdFilter != null) {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => ProductListScreen(
            categoryIdFilter: categoryIdFilter,
          ),
        ),
      );
    }
  }


  Future<List<Product>> _getProductsByCategoryId(int id) async {
    final productMaps = await helperProduct.queryAllProducts();
    return productMaps
        .map((productMap) => Product.fromMap(productMap))
        .where((product) => product.categorieId == id)
        .toList();
  }






  void _startEditing(Categorie category) {
    setState(() {
      _nom = category.nom;
      _editingCategoryId = category.id;
    });
  }

 /* void _clearForm() {
    setState(() {
      _nom = '';
      _editingCategoryId = null;
    });
    _formKey.currentState?.reset();
  }
*/
  TextDirection _detectTextDirection() {
    bool isArabicInput = _nom.contains(RegExp(r'[\u0600-\u06FF]'));
    return isArabicInput ? TextDirection.rtl : TextDirection.ltr;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          Container(
            width: 250,

            child: Center(
              child: SideMenu(

                controller: _sideMenuController,
                style: SideMenuStyle(
                  backgroundColor: Colors.deepPurple,
                  displayMode: SideMenuDisplayMode.auto,
                  hoverColor: Colors.white10,
                  selectedTitleTextStyle: TextStyle(color: Colors.white),
                  unselectedTitleTextStyle: TextStyle(color: Colors.white),
                  selectedIconColor: Colors.white,
                  unselectedIconColor: Colors.white70,
                  itemOuterPadding: EdgeInsets.symmetric(vertical: 8.0),
                ),

                title: Container(
                  height: 180.0,
                  child: Align(
                    alignment: Alignment.bottomCenter,

                  ),
                ),
                items: [
                  SideMenuItem(
                    title: 'Ajouter produit',
                    onTap: (index, controller) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AddProductScreen(),
                        ));
                    },
                    icon: Icon(Icons.add, color: Colors.white),
                  ),
                  SideMenuItem(
                    title: 'Categories',
                    onTap: (index, controller) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CategoryManagementScreen(),
                        ),
                      );
                    },
                    icon: Icon(Icons.category, color: Colors.white),
                  ),
                  SideMenuItem(
                    title: 'Produits',
                    onTap: (index, controller) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ProductListScreen(),
                      ));
                    },
                    icon: Icon(Icons.list, color: Colors.white),
                  ),
                  SideMenuItem(
                    title: 'Point de vente',
                    onTap: (index, controller) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SalesManagementScreen(),
                        ),
                      );
                    },
                    icon: Icon(Icons.shopping_cart_checkout, color: Colors.white),
                  ),
                  SideMenuItem(
                    title: 'Ventes',
                    onTap: (index, controller) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => OrderListScreen(),
                        ),
                      );
                    },
                    icon: Icon(Icons.sell, color: Colors.white),
                  ),
                  SideMenuItem(
                    title: 'Exit',
                    icon: Icon(Icons.exit_to_app, color: Colors.white),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        TextFormField(
                          initialValue: _nom,
                          decoration: InputDecoration(
                            hintText: 'Nom de la catégorie',
                          ),
                          validator: (value) {
                            final RegExp regex = RegExp(r'^[a-zA-Z\s]+$');
                            if (value == null || value.isEmpty) {
                              return 'Veuillez entrer le nom de la catégorie';
                            }
                            if (!regex.hasMatch(value.trim())) {
                              return 'Le nom de la catégorie ne doit contenir que des lettres et des espaces';
                            }
                            return null;
                          },
                          onSaved: (value) {
                            setState(() {
                              _nom = value ?? '';
                            });
                          },
                          textDirection: _detectTextDirection(),
                        ),


                        SizedBox(height: 16.0),
                        ElevatedButton(
                          onPressed: _saveCategory,
                          child: Text(_editingCategoryId == null ? "Ajouter catégorie" : "Mettre à jour catégorie"),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Expanded(
                    child: _categories.isNotEmpty
                        ? DataTable(
                      columns: [
                        DataColumn(label: Text('ID')),
                        DataColumn(label: Text('Nom de la catégorie')),
                        DataColumn(label: Text('Actions')),
                      ],
                      rows: _categories.map((category) {
                        return DataRow(cells: [
                          DataCell(Text(category.id.toString())),
                          DataCell(Text(category.nom)),
                          DataCell(
                            Row(
                              children: [
                                IconButton(
                                  icon: Icon(Icons.edit),
                                  onPressed: () {
                                    _startEditing(category);
                                  },
                                ),
                                IconButton(
                                  icon: Icon(Icons.delete),
                                  onPressed: () => _deleteCategory(category.id!),
                                ),
                              ],
                            ),
                          ),
                        ]);
                      }).toList(),
                    )
                        : Center(
                      child: Text('Aucune catégorie trouvée'),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
