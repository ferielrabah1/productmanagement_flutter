
import 'dart:convert';
import 'dart:async';
import 'package:collection/collection.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_crud_app/db/db_helperProduct.dart';
import 'package:flutter_crud_app/db/db_CategorieHelper.dart';
import 'package:flutter_crud_app/db/db_OrderHelper.dart';
import 'package:flutter_crud_app/models/product.dart';
import 'package:flutter_crud_app/models/categorie.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:open_file/open_file.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import '../../Repository/DataRepositoryImpl.dart';
import '../../Repository/DataRepositoryManager.dart';
import '../../Repository/RemoteDataRepository.dart';
import '../../db/DataToSync_helper.dart';
import '../../models/ProductsModified.dart';
import '../../models/order.dart';

import '../../services/connectivityService.dart';
import '../../services/serverAccessibilityService.dart';
import 'orders_list_screen.dart';

class SalesManagementScreen extends StatefulWidget {
  @override
  SalesManagementScreenState createState() => SalesManagementScreenState();
}

class SalesManagementScreenState extends State<SalesManagementScreen> {
  late List<Product> _products;
  late List<Product> _filteredProducts;
  late List<Categorie> _categories;
  late List<String> _categoryNames = [];
  List<Product> selectedProducts = [];
  double _amountPaid=0.0 ;
  double _totalOrder = 0.0;
  late double _rest;
  int _currentPage = 1;
  int _numclient =0;
  final int _productsPerPage = 12;
  int _selectedProductIndex = 1;
  Categorie? _selectedCategorie;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _controller = TextEditingController();
  final ProductHelper productHelper = ProductHelper();
  final CategorieHelper categorieHelper = CategorieHelper();
  final OrderHelper orderHelper = OrderHelper();
  final TextEditingController _searchController = TextEditingController();
  final FocusNode _searchFocusNode = FocusNode();
  final TextEditingController _restController = TextEditingController();
  final TextEditingController _amountPaidController = TextEditingController();
  final TextEditingController _numClientController = TextEditingController();
  Product? _editingProduct;
  final ConnectivityService connectivityService = ConnectivityService();
  bool _isConnectedMessageDisplayed = false;
  bool _isDisconnectedMessageDisplayed = false;
  late DataRepositoryManager repositoryManager;
  bool _isLoading = true;
  late Timer _syncTimer;
  bool _wasOffline = true;
  InternetStatus _previousInternetStatus = InternetStatus.disconnected;
  bool _showBtn = false;
  final DatatosyncHelper datatosyncHelper = DatatosyncHelper();
  final ServerAccessibilityService servAccess = ServerAccessibilityService(baseUrl: 'http://localhost:7001/api/products');


  double _progress = 0.0;
  bool _progBar = false;
  Map<int , int> productQuantities  ={};

  int _editingProductQuantity = 0;


  final TextEditingController _quantityController = TextEditingController();
  bool _local = false;
  String _userInput = '';
  final int maxDigits = 3;

  void _handleDigitPress(int digit) {
    setState(() {
      if (_userInput.length < maxDigits) {
        _userInput += digit.toString();
        _editingProductQuantity = int.tryParse(_userInput) ?? 0;

        // Update the quantity in real-time
        if (_editingProduct != null) {
          productQuantities[_editingProduct!.id!] = _editingProductQuantity;
          _totalOrder = _calculateTotalOrderForSelectedProducts();
        }
      }
    });
  }




  @override
  void initState() {
    super.initState();
    servAccess.startAutomaticChecking(Duration(seconds: 5));

    _products = [];
    _filteredProducts = [];
    _categories = [];
    _categoryNames = [];
//Add listener to the search controller to handle search changes
    _searchController.addListener(_onSearchChanged);
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      _searchFocusNode.requestFocus();
    });

      // Initialize the connectivity service
    connectivityService.initialize();
    // Set up local and remote data repositories
    final localRepository = LocalDataRepository(productHelper);
    final remoteRepositoryManager = RemoteDataRepository(apiUrl: 'http://localhost:7001/api');
    // Initialize the repository manager with the local and remote repositories and the connectivity service
    repositoryManager = DataRepositoryManager(
      localRepository: productHelper,
      remoteRepository: remoteRepositoryManager,
      cnxSrv: connectivityService,
    );


    // Listen to connectivity status changes
    connectivityService.subscription.onData((InternetStatus status) async {
      // Check for connection existence
      if (status == InternetStatus.connected && await servAccess.checkServerAccessibility()) {

      //  print('Server is reachable and internet is connected.');
        setState(() {
          _isLoading = true;
        });
        // Check for server accessibility

        if (!_isConnectedMessageDisplayed) {
          _isConnectedMessageDisplayed = true;
          _isDisconnectedMessageDisplayed = false;
          _switchToRemoteWithDialog('Vous êtes maintenant connecté', context);

          _syncTimer = Timer.periodic(Duration(seconds: 5), (timer) async {

              await synchronizeLocalChangesWithRemote(
                  remoteRepositoryManager, datatosyncHelper);
              setState(() {
                _isLoading = true;
              });
             await _loadProductsFromRemote();
              await synchronizeRemoteChangesWithLocal();


          });
        }
      }
        if (status == InternetStatus.connected && !await servAccess.checkServerAccessibility()) {
       //   print('Server is not reachable.');



          if (!_isDisconnectedMessageDisplayed) {
            _isDisconnectedMessageDisplayed = true;
            _isConnectedMessageDisplayed = false;
            setState(() {
              _isLoading = false;
            });
            _loadProducts();
            _switchToLocalWithDialog(
                'Vous pouvez continuer à utiliser l\'application.', context);
          }
        }


      if (status != InternetStatus.connected) {
        if (!_isDisconnectedMessageDisplayed) {
          _isDisconnectedMessageDisplayed = true;
          _isConnectedMessageDisplayed = false;
          setState(() {
            _isLoading = false;

          });
          _loadProducts();
          _switchToLocalWithDialog('Vous pouvez continuer à utiliser l\'application.', context);
          _syncTimer.cancel();
        }
      }

      _wasOffline = status != InternetStatus.connected;
    });

  }

  //clean up controllers and listeners
  @override
  void dispose() {
  _searchController.removeListener(_onSearchChanged);
  _searchController.dispose();
  _amountPaidController.dispose();
  _restController.dispose();
  connectivityService.dispose();
  _syncTimer.cancel();
  servAccess.stopCheckingServerAccessibility();


  super.dispose();
  }
// Function to show a dialog with a message while switching to local
  void _switchToLocalWithDialog(String message, BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Passage en mode hors ligne",
            style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
          ),
          content: Text(message),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text("OK"),
            ),
          ],
        );
      },
    );
  }
  // Function to show a dialog with a message while switching to remote

  void _switchToRemoteWithDialog(String message, BuildContext context) {
    if (connectivityService.isConnected) {

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(

            content: Text(message),
            actions: <Widget>[
              TextButton(
                child: Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }
  // Searching for a product using barcode
  void _onSearchChanged() {
    setState(() {
      // Filter products by barcode
      _filteredProducts = _products
          .where((product) =>
          product.barCode.toLowerCase().contains(_searchController.text.toLowerCase()))
          .toList();
      _currentPage = 1;
    });
  }
// setup and return a GraphQL client instance
  GraphQLClient setupGraphQLClient() {
    final HttpLink httpLink = HttpLink('http://127.0.0.1:3000/graphql');
    return GraphQLClient(
      link: httpLink,
      cache: GraphQLCache(),
    );
  }
// GraphQL query as a constant string for retrieving products
  static const String queryProducts = r'''
  query {
    products {
      id_prod
      name
      description
      price
      image_url
      categorie_id
      quantity_purchased
      stock
      bar_code
      deleted
    }
  }
  ''';
// Function to query products from the GraphQL server and return a list of Product objects
  Future<List<Product>> queryProductsFromGraphQLServer() async {
    final GraphQLClient client = setupGraphQLClient();

    final QueryOptions options = QueryOptions(
      document: gql(queryProducts),
    );

    final QueryResult result = await client.query(options);

    if (result.hasException) {
      throw result.exception!;
    }

    final productsData = result.data!['products'] as List<dynamic>;
    print(productsData);
    return productsData.map((productJson) => Product.fromGraphQLMap(productJson)).toList();
  }


// Fetch all products from remote
  Future<void> _loadProductsFromRemote() async {
    try {
      List<Product> fetchedProducts = await repositoryManager.remoteRepository.fetchProducts();
      setState(() {
        _products = fetchedProducts;
        _filteredProducts = fetchedProducts;
        _isLoading = false;
      });
      print("Products loaded from remote successfully");
    } catch (e) {
      print("Error fetching products from remote: $e");
    }
  }
  // fetch all products from local
  Future<void> _loadProducts() async {

    setState(() {
      _local = true;
    });
      final List<Map<String, dynamic>> productMaps = await productHelper.queryAllProducts();
        setState(() {
          _products = productMaps.map((productMap) => Product.fromMap(productMap)).toList();
          _filteredProducts = List.from(_products);
        });
      _onSearchChanged();
  }

//Function to upload products from file in case local is empty

  Future<void> _uploadFromFile() async {
    try {

      final List<Map<String, dynamic>> productMaps = await productHelper.queryAllProducts();
      if(productMaps.isNotEmpty){
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text('Il existe déja des produits en local!'),
            duration: Duration(seconds: 2),
          ),

        );
      } else
        {
          FilePickerResult? result = await FilePicker.platform.pickFiles(
            type: FileType.custom,
            allowedExtensions: ['json'],
          );
          setState(() {
            _progBar = true;
            _progress = 0.0;
          //  _upload = true;


          });

          if (result != null && result.files.single.path != null) {
            final file = File(result.files.single.path!);


            final parentDirectory = file.parent;

            // Simulate incremental progress
            Timer.periodic(Duration(milliseconds: 100), (timer) async {
              if (_progress < 100.0) {
                setState(() {
                  _progress += 10.0;
                  if (_progress > 100.0) {
                    _progress = 100.0;
                  }
                });
              } else {
                timer.cancel();
                setState(() {
                  _progBar = false;
                });

                // Process the file (simulate reading and processing)
                final fileContent = await file.readAsString();
                final productsFile = await fetchProductsFromJsonString(fileContent);

                // Fetch existing products
                final List<Map<String, dynamic>> productMaps = await productHelper.queryAllProducts();


                // Insert products from file if there are no existing products
                if (productMaps.isEmpty) {
                  for (var product in productsFile) {
                    if (product.imageUrl != null) {
                      // Decode the base64 image string and save it to a file
                      final savedImagePath = await _saveImageFromBase64(product.imageUrl , product , product.name);

                      // Update the product with the path of the saved image
                      product.imageUrl = savedImagePath;
                    }

                    // Insert the product into the repository
                    await repositoryManager.localRepository.insertProduct(product);
                    print("Product fetched from file and saved");
                  }

                  // Refresh product maps after inserting the products
                  final updatedProductMaps = await productHelper.queryAllProducts();
                  setState(() {
                    _products = updatedProductMaps.map((productMap) => Product.fromMap(productMap)).toList();
                    _filteredProducts = List.from(_products);
                    _showBtn = true;
                  });
                  _onSearchChanged();
                }
              }
            });
          } else {
            setState(() {
              _progBar = false;
            });
            print('No file selected');
          }
        }
      // Let the user select a file

    } catch (e) {
      print('Error uploading file: $e');
      setState(() {
        _progBar = false;

      });
    }
  }

  Future<String> _saveImageFromBase64(String base64String , Product p , String name) async {
    try {

      final imageData = base64String.contains(',')
          ? base64String.split(',').last
          : base64String;


      final cleanedBase64String = imageData.trim();

      // Decode the base64 string to bytes
      final Uint8List imageBytes = base64Decode(cleanedBase64String);


      final directory = await getApplicationDocumentsDirectory();
      final mediaDirectory = Directory('${directory.path}/mediaRessources');


      if (!await mediaDirectory.exists()) {
        await mediaDirectory.create();
      }


      final imagePath = '${mediaDirectory.path}/${p.name}.jpg';
      final imageFile = File(imagePath);

      // Save the image file
      await imageFile.writeAsBytes(imageBytes);
      print('Image saved at $imagePath');
      return imagePath;
    } catch (e) {
      print('Error saving image: $e');
      return '';
    }
  }

// Update fetchProductsFromJsonFile to accept a JSON string
  Future<List<Product>> fetchProductsFromJsonString(String jsonString) async {
    final List<dynamic> jsonData = jsonDecode(jsonString);
    return jsonData.map((json) => Product.fromJson(json)).toList();
  }

  void _updateSelectedProductQuantity() {
    int newQuantity = int.tryParse(_userInput) ?? 0;

    setState(() {
      if (_editingProduct != null) {
        if (_editingProduct!.id != null) {
          productQuantities[_editingProduct!.id!] = newQuantity;
        }
      } else if (selectedProducts.isNotEmpty) {
        int lastIndex = selectedProducts.length - 1;
        Product lastProduct = selectedProducts[lastIndex];

        if (lastProduct.id != null) {
          productQuantities[lastProduct.id!] = newQuantity;
        }
      }

      _totalOrder = _calculateTotalOrderForSelectedProducts();

      // Clear the editing state and user input
      _editingProduct = null;
      _userInput = '';
    });
  }


  //Calculate total order
  double _calculateTotalOrderForSelectedProducts() {
    double total = 0;
    for (var product in selectedProducts) {
      int quantity = productQuantities[product.id!] ?? 0;
      total += product.price * quantity;
    }
    return total;
  }

// Add a product to an order
  void _addProductToOrder(Product product, int quantity) {
    if (product.stock < quantity) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Stock épuisé'),
          duration: Duration(seconds: 2),
        ),
      );
      return;
    }

    setState(() {
      final existingProductIndex = selectedProducts.indexWhere((p) => p.id == product.id);
      if (existingProductIndex != -1) {
        // Update quantity in productQuantities map
        productQuantities[product.id!] = (productQuantities[product.id!] ?? 0) + quantity;
      } else {
        // Add new product and quantity
        selectedProducts.add(product);
        productQuantities[product.id!] = quantity;
      }
      _totalOrder = _calculateTotalOrderForSelectedProducts();
    });
  }
  void _quickAddProductToOrder(Product product) {
    _addProductToOrder(product, 1);
  }
  // Updating rest value
  void _updateRest() {
    setState(() {
      _rest = _amountPaid - _totalOrder;
      _restController.text = _rest.toStringAsFixed(3);
    });
  }
  // Save order into db
  Future<void> _saveOrder() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      if (selectedProducts.isNotEmpty) {
        try {
          Order order = Order(
            products: selectedProducts,
            totalOrder: _totalOrder,
            amountPaid: _amountPaid,
            numClient: _numclient,
            rest: _rest,
            date: DateTime.now(),
            productQuantities: productQuantities,
          );

          await orderHelper.insertOrder(order);


          for (var product in selectedProducts) {
            product.stock -= productQuantities[product.id!]!;
            await repositoryManager.localRepository.updateProduct(product);
            await repositoryManager.remoteRepository.updateProduct(product);
          }

          _updateRest();
          resetOrder();

          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text('Commande enregistrée!'),
              duration: Duration(seconds: 2),
            ),
          );
        } catch (e) {
          print('Failed to save order: $e');
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text('Erreur lors de l\'enregistrement de la commande'),
              duration: Duration(seconds: 2),
            ),
          );
        }
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text('Veuillez remplir tous les champs requis'),
          duration: Duration(seconds: 2),
        ),
      );
    }
  }


// Sync remote to local
  Future<void> synchronizeRemoteChangesWithLocal() async {
    try {
      // Fetch the list of modified products to sync from the remote repository
      List<ProductsModified> remoteProductsModified = await repositoryManager.remoteRepository.fetchProductsToSyncFromRemote();

      print('Remote products modified:');
      for (var product in remoteProductsModified) {
        print('- Product ID: ${product.productId}, Operation: ${product.typeOperation}');
      }

      for (var remoteProductModified in remoteProductsModified) {
        int productId = remoteProductModified.productId;

        try {
          if (remoteProductModified.sync == false) {
            if (remoteProductModified.typeOperation == 'CREATE') {
              // Handle CREATE operation
              Product remoteProduct = await repositoryManager.remoteRepository
                  .fetchProductById(productId);
              var response = await http.get(Uri.parse(remoteProduct.imageUrl));
              if (response.statusCode == 200) {
                Directory appDocDir = await getApplicationDocumentsDirectory();
                String appDocPath = appDocDir.path;
                String fileName = '${remoteProduct.name}.jpeg';
                String localImagePath = '$appDocPath/$fileName';
                File localImageFile = File(localImagePath);
                await localImageFile.writeAsBytes(response.bodyBytes);

                remoteProduct.imageUrl = localImagePath;
                await repositoryManager.localRepository.insertProduct(
                    remoteProduct);
                await repositoryManager.remoteRepository
                    .updateProductSyncStatus(remoteProductModified);
                print('Added product ${remoteProduct.name} to local');
              }

              if (remoteProductModified.typeOperation == 'PATCH') {
                // Handle PATCH operation for existing products
                Product? existingProduct = await repositoryManager
                    .localRepository.getProductById(productId);
                if (existingProduct != null) {
                  // Fetch the detailed remote product information using productId
                  Product remoteProduct = await repositoryManager
                      .remoteRepository.fetchProductById(productId);
                  existingProduct.stock = remoteProduct.stock;
                  existingProduct.name = remoteProduct.name;
                  await repositoryManager.localRepository.updateProduct(
                      existingProduct);
                  await repositoryManager.remoteRepository
                      .updateProductSyncStatus(remoteProductModified);

                  print('Updated product ${remoteProduct.name} locally');
                } else {
                  continue;
                  //  print('Product with ID $productId does not exist locally, skipping PATCH operation');
                }
              }

              if (remoteProductModified.typeOperation == 'DELETE') {
                // Handle DELETE operation
                int productToDelete = remoteProductModified.productId;
                await repositoryManager.localRepository.deleteProduct(
                    productToDelete);
                await repositoryManager.remoteRepository
                    .updateProductSyncStatus(remoteProductModified);

                print('Deleted product with ID $productId from local');
              }
            } else {
              print(
                  'Operation for product with ID $productId has already been synchronized, skipping...');
            }
          }
          // Delete the processed operation from the remote repository
          // await repositoryManager.remoteRepository.deleteOpreation(remoteProductModified.id);
        } catch (e) {
          print('Error processing product ID $productId: $e');
        }
      }

      print('Data synchronization from remote to local completed.');
    } catch (e) {
      print('Error synchronizing data from remote to local: $e');
      throw Exception('Failed to synchronize data from remote to local');
    }
  }
  // Sync local to remote
  Future<void> synchronizeLocalChangesWithRemote(RemoteDataRepository remoteRepository, DatatosyncHelper dataSyncHelper,) async {

    // 1. Récupérer tous les produits de la base de données locale
    final List<Map<String, dynamic>> localProductMaps = await productHelper.queryAllProducts();
    final List<Product> localProducts = localProductMaps.map((productMap) => Product.fromMap(productMap)).toList();

    // 2. Récupérer tous les produits du serveur distant
    final List<Product> fetchedProducts = await repositoryManager.remoteRepository.fetchProducts();

    // 3. Extraire les identifiants des produits distants et les convertir en chaînes
    final Set<String> remoteProductIds = fetchedProducts
        .map((product) => product.id.toString())  // Conversion des IDs en chaînes
        .toSet();

    // 4. Identifier les produits locaux dont les identifiants ne sont pas présents à distance
    final List<Product> productsToDelete = localProducts.where((product) {
      // Conversion de l'ID local en chaîne pour la comparaison
      return !remoteProductIds.contains(product.id.toString());
    }).toList();

    // 5. Supprimer les produits locaux qui ne sont pas présents sur le serveur
    for (var product in productsToDelete) {
      await productHelper.deleteProductPH(product.id!);
      print("Produit local supprimé : ${product.id}");
    }

    // Optionnel : Rafraîchir la liste des produits locaux après suppression
    final updatedProductMaps = await productHelper.queryAllProducts();
    setState(() {
      _products = updatedProductMaps.map((productMap) => Product.fromMap(productMap)).toList();
      _filteredProducts = List.from(_products);
    });


    final dataToSyncList = await dataSyncHelper.getAllDatatosyncProducts();

    for (final dataToSyncItem in dataToSyncList) {
      print("id itemmmm : ${dataToSyncItem.id}");
      if (dataToSyncItem.sync == 0){
        for (final productId in dataToSyncItem.products) {
          final localProduct = await repositoryManager.localRepository.getProductById(productId as int);

          if (localProduct != null) {
            final remoteProduct = await remoteRepository.fetchProductById(productId);

            if (remoteProduct != null) {
              remoteProduct.stock -=  localProduct.quantityPurchased;
              await remoteRepository.updateProduct(remoteProduct);
              print("Stock updated for ${remoteProduct.name}");
            } else {
              print("No matching remote product found for product ID $productId");
            }
          } else {
            print("No local product found for product ID $productId");
          }
        }
        datatosyncHelper.updateDataToSyncProduct(dataToSyncItem.id!, 1);
      }

    }
   // await dataSyncHelper.clearDataToSyncProducts();
    print('Synchronization local to remote completed.');
  }


  void resetOrder() {
    setState(() {
      _amountPaidController.text = '' ;
      selectedProducts = [];
      _totalOrder = 0.0;
      _restController.text = '';
      _numClientController.text = '';
      _rest = double.tryParse(_restController.text) ?? 0.0;
    });
  }

  Future<void> generateOrderSummary(BuildContext context) async {
    DateTime currentDateTime = DateTime.now();
    String formattedTime = DateFormat('kk:mm').format(currentDateTime);
    String formattedDate = DateFormat('yyyy-MM-dd').format(currentDateTime);

    final pdf = pw.Document();
    final pageFormat = PdfPageFormat.a4.copyWith(
      width: PdfPageFormat.a4.width / 2.5,
      marginLeft: 5,
      marginRight: 5,
      marginTop: 10,

      height: 350,
      //marginBottom: 10,
    );
    pdf.addPage(
      pw.Page(
        pageFormat: pageFormat,
        build: (pw.Context context) {
          return pw.Column(
            crossAxisAlignment: pw.CrossAxisAlignment.start,
            mainAxisSize: pw.MainAxisSize.min,
            children: [
              // Ticket Title and Date/Time
              pw.Text(
                'Ticket',
                style: pw.TextStyle(
                  fontSize: 16,
                  fontWeight: pw.FontWeight.bold,
                ),
              ),
              pw.SizedBox(height: 5),
              pw.Text(
                'Date: $formattedDate',
                style: pw.TextStyle(
                  fontSize: 10,
                ),
              ),
              pw.Text(
                'Time: $formattedTime',
                style: pw.TextStyle(
                  fontSize: 10,
                ),
              ),
              pw.SizedBox(height: 10),

              // Table Header
              pw.Row(
                mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                children: [
                  // Produit
                  pw.Container(
                    width: 60,
                    child: pw.Text(
                      'Produit',
                      style: pw.TextStyle(
                        fontSize: 8,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),
                  // Quantité
                  pw.Container(
                    width: 50,
                    child: pw.Text(
                      'Quantité',
                      style: pw.TextStyle(
                        fontSize: 8,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),
                  // PU
                  pw.Container(
                    width: 50,
                    child: pw.Text(
                      'PU',
                      style: pw.TextStyle(
                        fontSize: 8,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),
                  // PT
                  pw.Container(
                    width: 50,
                    child: pw.Text(
                      'PT',
                      style: pw.TextStyle(
                        fontSize: 8,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              pw.SizedBox(height: 5),
              // Ligne de séparation
              pw.Container(
                height: 1,
                color: PdfColors.black,
                width: double.infinity,
              ),
              pw.SizedBox(height: 5),

              // Products List
              for (var product in selectedProducts)
                pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                  children: [
                    // Produit
                    pw.Container(
                      width: 60,
                      child: pw.Text(
                        '${insertNewlines(product.name, 2)}',
                        maxLines: 3,
                        style: pw.TextStyle(
                          fontSize: 8,
                        ),
                      ),
                    ),
                    // Quantité
                    pw.Container(
                      width: 50,
                      child: pw.Text(
                     '${productQuantities[product.id]}',
          style: pw.TextStyle(
                          fontSize: 8,
                        ),
                      ),
                    ),
                    // PU
                    pw.Container(
                      width: 50,
                      child: pw.Text(
                        '${product.price.toStringAsFixed(3)} DT',
                        style: pw.TextStyle(
                          fontSize: 8,
                        ),
                      ),
                    ),
                    // PT
                    pw.Container(
                      width: 50,
                      child: pw.Text(
                        '${(product.price * productQuantities[product.id]!).toStringAsFixed(3)} DT',
                        style: pw.TextStyle(
                          fontSize: 8,
                        ),
                      ),
                    ),
                     pw.SizedBox(height: 25)
                  ],

                ),
               pw.SizedBox(height: 15),

              // Total Commande
              pw.Row(
                children: [
                  pw.Expanded(
                    child: pw.Text(
                      'Total Commande',
                      style: pw.TextStyle(
                        fontSize: 10,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),
                  pw.Text(
                    '${_totalOrder.toStringAsFixed(3)} DT',
                    style: pw.TextStyle(
                      fontSize: 10,
                      fontWeight: pw.FontWeight.bold,
                    ),
                  ),
                ],
              ),
              pw.Row(
                children: [
                  pw.Expanded(
                    child: pw.Text(
                      'ESPECES',
                      style: pw.TextStyle(
                        fontSize: 10,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),
                  pw.Text(
                    '${_amountPaid.toStringAsFixed(3)} DT',
                    style: pw.TextStyle(
                      fontSize: 10,
                      fontWeight: pw.FontWeight.bold,
                    ),
                  ),
                ],
              ),


              pw.Row(
                children: [
                  pw.Expanded(
                    child: pw.Text(
                      'RESTE',
                      style: pw.TextStyle(
                        fontSize: 10,
                        fontWeight: pw.FontWeight.bold,
                      ),
                    ),
                  ),
                  pw.Text(
                    '${_rest.toStringAsFixed(3)} DT',
                    style: pw.TextStyle(
                      fontSize: 10,
                      fontWeight: pw.FontWeight.bold,
                    ),
                  ),
                ],
              ),

            ],
          );
        },
      ),


    );

    final tempDir = await getTemporaryDirectory();
    final tempPath = tempDir.path;
    final tempFilePath = '$tempPath/order_summary.pdf';
    final File file = File(tempFilePath);
    await file.writeAsBytes(await pdf.save());

    await OpenFile.open(tempFilePath);
    await Printing.layoutPdf(
      onLayout: (PdfPageFormat format) async => pdf.save(),
    );
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text('Résumé de commande généré et ouvert!'),
        duration: Duration(seconds: 2),
      ),
    );
  }
  String insertNewlines(String text, int n) {
    StringBuffer sb = StringBuffer();
    int count = 0;

    for (int i = 0; i < text.length; i++) {
      if (text[i] == ' ') {
        count++;
        sb.write(' ');
        if (count == n) {
          sb.write('\n');
          count = 0;
        }
      } else {
        sb.write(text[i]);
      }
    }

    return sb.toString();
  }

  List<Product> _getPaginatedProducts() {
    final startIndex = (_currentPage - 1) * _productsPerPage;
    final endIndex = startIndex + _productsPerPage;
    return _filteredProducts.sublist(
      startIndex,
      endIndex > _filteredProducts.length ? _filteredProducts.length : endIndex,
    );
  }
  List<Product> _getFilteredProducts() {
    if (_selectedCategorie != null) {
      return _products
          .where((product) => product.categorieId == _selectedCategorie!.id)
          .toList();
    } else {
      return _products;
    }
  }
  Future<void> generateOrdersPdf() async {
    // Fetch orders for today
    final orders = await orderHelper.fetchOrdersForToday();
    final totalAmount = orders.fold<double>(0.0, (sum, order) => sum + order.totalOrder);
    // Create a PDF document
    final pdf = pw.Document();

    // Add a page to the PDF document
    pdf.addPage(
      pw.Page(
        build: (pw.Context context) {
          return pw.Column(
            crossAxisAlignment: pw.CrossAxisAlignment.start,
            children: [

              pw.Text('Récapitulatif quotidien des ventes', style: pw.TextStyle(fontSize: 12)),
              pw.SizedBox(height: 20),

              // Display each order
              ...orders.map(
                    (order) => pw.Column(
                  crossAxisAlignment: pw.CrossAxisAlignment.start,
                  children: [
                    pw.Text('Order ID: ${order.id}', style: pw.TextStyle(fontSize: 12, fontWeight: pw.FontWeight.bold)),
                    pw.SizedBox(height: 10),

                    ...order.productQuantities.entries.map(
                          (entry) {
                        final productName = order.products.firstWhere((p) => p.id == entry.key).name;
                        return pw.Text(
                          ' $productName, x ${entry.value}',
                          style: pw.TextStyle(fontSize: 12),
                        );
                      },
                    ),
                    pw.Text('Total : ${order.totalOrder}', style: pw.TextStyle(fontSize: 12, fontWeight: pw.FontWeight.bold)),

                    // Add a line separator
                    pw.SizedBox(height: 10),
                    pw.Divider(height: 2, color: PdfColors.black),
                    pw.SizedBox(height: 10),
                  ],
                ),
              ),

              // Add total orders
              pw.SizedBox(height: 20),
              pw.Text('Total Orders : ${totalAmount.toStringAsFixed(2)} DT', style: pw.TextStyle(fontSize: 14, fontWeight: pw.FontWeight.bold)),
            ],
          );
        },
      ),
    );
    final tempDir = await getTemporaryDirectory();
    final tempPath = tempDir.path;
    final timestamp = DateTime.now().toIso8601String().replaceAll(':', '-');
    final tempFilePath = '$tempPath/orders_${timestamp}.pdf';
    final File file = File(tempFilePath);
    await file.writeAsBytes(await pdf.save());

    await OpenFile.open(tempFilePath);
    await Printing.layoutPdf(
      onLayout: (PdfPageFormat format) async => pdf.save(),
    );




    print('Orders PDF file generated at $tempFilePath');
  }
  bool _showProducts = false;
  late Future<List<Product>> _productsFuture;


  @override
  Widget iConMenu() {
  return PopupMenuButton<String>(
  icon: Icon(Icons.settings, size: 30),
  onSelected: (String result) {
    if (result == 'Upload'){
      _uploadFromFile();
    }
    else if (result == 'ventes') {
      /*Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => OrderListScreen(),
        ),
      );*/
      generateOrdersPdf();
    } else if (result == 'graphQl') {
      setState(() {
        _productsFuture = queryProductsFromGraphQLServer();
        _showProducts = true;
      });    }

    },
  itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
  PopupMenuItem<String>(
  value: 'Upload',
  child: Text('Upload'),
  ),
  PopupMenuItem<String>(
  value: 'ventes',
  child: Text('Ventes du jour'),
  ),
    PopupMenuItem( value: 'graphQl',
      child: Text('graphQl') )

  ],
  );
  }


  Widget _buildProductList() {
    return FutureBuilder<List<Product>>(
      future: _productsFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
          return Center(child: Text('No products found.'));
        } else {
          final products = snapshot.data!;
          return GridView.builder(
            padding: EdgeInsets.all(10.0),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 5,
              childAspectRatio: 0.8 / 1,
              mainAxisSpacing: 5.0,
              crossAxisSpacing: 5.0,
            ),
            itemCount: products.length,
            itemBuilder: (context, index) {
              final product = products[index];
              return GestureDetector(
                onTap: () {
                  setState(() {
                    _editingProduct = product;
                    _selectedProductIndex = index;
                    _quickAddProductToOrder(product);
                  });
                },
                child: Card(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: ClipRRect(
                          borderRadius: BorderRadius.vertical(top: Radius.circular(12.0)),
                          child: Stack(
                            children: [
                              Visibility(
                                visible: product.imageUrl.isNotEmpty,
                                child: Image.network(
                                  product.imageUrl,
                                  width: double.infinity,
                                  fit: BoxFit.cover,
                                  errorBuilder: (context, error, stackTrace) => Container(
                                    color: Colors.grey[200],
                                    child: Icon(Icons.error, color: Colors.grey[400]),
                                  ),
                                ),
                              ),
                              Visibility(
                                visible: product.imageUrl.isEmpty,
                                child: Container(
                                  color: Colors.grey[200],
                                  child: Icon(Icons.image, size: 50, color: Colors.grey[400]),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              product.name,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            SizedBox(height: 4.0),
                            Text(
                              'Prix: ${product.price.toStringAsFixed(2)} DT',
                              style: TextStyle(
                                fontSize: 12,
                              ),
                            ),
                            SizedBox(height: 4.0),
                            Text(
                              'Stock: ${product.stock}',
                              style: TextStyle(
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        }
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    final paginatedProducts = _getPaginatedProducts();
    final totalOrder = _calculateTotalOrderForSelectedProducts();
    return Scaffold(
      backgroundColor: Colors.white,

      body:
      Row(

        children: [


        Container(
            color: Colors.white,
            width: 80,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                 Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: iConMenu()
                  ),


              ],
            ),
          ),




          Expanded(

            flex: 2,
            child: Container(
              height: 650,
              width: 500,
              margin: const EdgeInsets.only(left: 10.0, right: 50.0),
              decoration: BoxDecoration(
               // color: Colors.grey[200],
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(

                children: [


                    //padding: const EdgeInsets.only(left: 500.0 , top: 15.0),
                  Container(
                    margin: const EdgeInsets.only(left: 500.0, top: 15.0),
                    width: 300,
                    child: TextFormField(
                      controller: _searchController,
                      focusNode: _searchFocusNode,
                      decoration: InputDecoration(
                        hintText: 'Rechercher produit...',
                        prefixIcon: Icon(Icons.search),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                      ),
                      onFieldSubmitted: (value) {
                        _onSearchChanged();
                      },

                    ),
                  ),



                  SizedBox(height: 10),
                  Expanded(

                    child: _showProducts
                        ? _buildProductList()
                        : _isLoading
                        ? Center(
                      child: Container(
                        height: 100,
                        child: Center(
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              // Progress indicator
                              _isLoading && connectivityService.isConnected
                                  ? Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CircularProgressIndicator(
                                    backgroundColor: Colors.deepPurple[200],
                                    color: Colors.blue,
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    'Synchronisation en cours...',
                                    style: TextStyle(fontSize: 16, color: Colors.black),
                                  ),
                                ],
                              )
                                  : Container(),
                            ],
                          ),
                        ),
                      ),
                    )
                    : _progBar
                        ? Center(
                      child: Container(
                        width: 300,
                        height: 100,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                width: 500,
                                child: LinearProgressIndicator(value: _progress / 100),
                              ),
                              SizedBox(height: 8.0),
                              Text(
                                '${_progress.toStringAsFixed(2)}%',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 16.0),
                            ],
                          ),
                        ),
                      ),
                    )
                        : paginatedProducts.isEmpty
                        ? Center(
                      child: Text(
                        'Aucun produit disponible',
                        style: TextStyle(fontSize: 18, color: Colors.grey),
                      ),
                    )
                        : GridView.builder(
                      padding: EdgeInsets.all(10.0),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 5,
                        childAspectRatio: 0.8 / 1,
                        mainAxisSpacing: 5.0,
                        crossAxisSpacing: 5.0,
                      ),
                      itemCount: paginatedProducts.length,
                      itemBuilder: (context, index) {
                        final product = paginatedProducts[index];
                        return GestureDetector(
                          onTap: () {
                            setState(() {
                              _editingProduct = product;
                              _selectedProductIndex = index;
                              _quickAddProductToOrder(product);
                            });
                          },
                          child: Card(
                            elevation: 4,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)),
                                    child: Stack(
                                      children: [
                                        Visibility(
                                          visible:  _local && product.imageUrl.isNotEmpty  ,
                                          child: Image.file(
                                            File(product.imageUrl),
                                            width: double.infinity,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        Visibility(
                                          visible:  product.imageUrl.isNotEmpty,
                                          child: Image.network(
                                            product.imageUrl,
                                            width: double.infinity,
                                            fit: BoxFit.cover,
                                            errorBuilder: (context, error, stackTrace) => Container(
                                              color: Colors.grey[200],
                                              child: Icon(Icons.error, color: Colors.grey[200]),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),








                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        product.name,
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14,
                                        ),
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      SizedBox(height: 4.0),
                                      Text(
                                        'Prix: ${product.price.toStringAsFixed(3)} DT',
                                        style: TextStyle(
                                          fontSize: 12,
                                        ),
                                      ),
                                      SizedBox(height: 4.0),
                                      Text(
                                        'Stock: ${product.stock}',
                                        style: TextStyle(
                                          fontSize: 12,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),

                  Container(
                    margin: EdgeInsets.only(left: 300),
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [

                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton.icon(
                                onPressed: _saveOrder,
                                icon: Icon(Icons.save),
                                label: Text('Enregistrer'),
                              ),
                              SizedBox(width: 16),
                              ElevatedButton.icon(
                                onPressed: () {
                                  generateOrderSummary(context);
                                },
                                icon: Icon(Icons.print),
                                label: Text('Imprimer'),
                              ),
                              SizedBox(width: 16),

                            ],
                          ),
                        ],
                      ),
                    ),
                  )



                ],
              ),
            ),

          ),
          Expanded(
            flex: 1,

            child: Container(
            //  margin: const EdgeInsets(right : 50.0),
              height: 800,
              decoration: BoxDecoration(
                border: Border(
                  left: BorderSide(width: 1, color: Colors.deepPurple),
                ),
              ),

              child: Column(
                children: [
              Expanded(
              child: Column(
              children: [
                  Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(left: 20 , top: 20.0),
                    width: 600,


      child: SingleChildScrollView(
        child: Column(
          children: [
            Table(
              border: TableBorder.symmetric(),
              children: [
                TableRow(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),

                      child: Text('Produit', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.deepPurple)),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('PU', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.deepPurple)),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('QT', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.deepPurple)),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                    ),
                  ],
                ),
                ...selectedProducts.map((product) {
                  bool isEditing = _editingProduct != null && _editingProduct!.id == product.id;

                  return TableRow(
                    decoration: BoxDecoration(
                      color: isEditing ? Color.fromARGB(50, 0, 0, 0) : Colors.transparent,
                    ),
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(product.name, style: TextStyle(color: Colors.black)),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('${product.price.toStringAsFixed(3)} DT', style: TextStyle(color: Colors.black)),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('${productQuantities[product.id!] ?? 0}', style: TextStyle(color: Colors.black)),
                      ),

                      GestureDetector(
                        onTap: () {
                          // Show a confirmation dialog before removing the product
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text('Confirmation'),
                                content: Text('Êtes-vous sûr de vouloir supprimer ce produit ?'),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () {
                                      setState(() {
                                        selectedProducts.remove(product);
                                        _totalOrder -= product.totalPrice;
                                        _userInput = _editingProductQuantity.toString();
                                      });
                                      Navigator.of(context).pop();
                                    },
                                    child: Text('Yes'),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Text('No'),
                                  ),
                                ],
                              );
                            },
                          );
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(Icons.cancel, color: Colors.red, size: 25),
                        ),
                      ),

                      GestureDetector(
                        onTap: () {
                          setState(() {
                            _editingProduct = product;
                            _editingProductQuantity = productQuantities[product.id!]!;
                          });
                        },
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(Icons.edit, color: Colors.deepPurple , size: 25),
                        ),
                      ),

                    ],
                  );
                }).toList(),
              ],
            ),

            SizedBox(height: 20),
          ],
        ),
      ),
    ),
    ),
                Row(
                  children: [
                    Expanded(
                      child:  Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                    child : Padding(
                                      padding : const EdgeInsets.only(left: 10.0),

                                      child: TextFormField(
                                        controller: _amountPaidController,

                                        decoration: InputDecoration(
                                          hintText: 'ESPECES',
                                          fillColor: Colors.white,
                                          hintStyle: TextStyle(color: Colors.deepPurple),
                                          border: UnderlineInputBorder(
                                            borderSide: BorderSide(color: Colors.deepPurple),
                                          ),
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(color: Colors.deepPurple),
                                          ),
                                          focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(color: Colors.deepPurple),
                                          ),
                                        ),
                                        style: TextStyle(color: Colors.deepPurple),
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Veuillez entrer le montant fourni';
                                          }
                                          if (double.tryParse(value.replaceAll(',', '.')) == null) {
                                            return 'La valeur du prix doit être numérique';
                                          }
                                          return null;
                                        },
                                        onChanged: (value) {
                                          if (value != null) {
                                            setState(() {
                                              _amountPaid = double.parse(value);
                                            });
                                            _updateRest();
                                          }
                                        },
                                        onSaved: (value) {
                                          if (value != null) {
                                            setState(() {
                                              _amountPaid = double.parse(value);
                                            });
                                            _updateRest();
                                          }
                                        },
                                      ),

                                    )

                                ),

                                SizedBox(width: 10),
                                Padding(
                                  padding: const EdgeInsets.only(right: 10.0),
                                  child: SizedBox(
                                    width: 150,
                                    child: TextFormField(
                                      readOnly: true,
                                      controller: _restController,
                                      decoration: InputDecoration(
                                        hintText: 'RESTE',
                                        fillColor: Colors.deepPurple,
                                        hintStyle: TextStyle(color: Colors.deepPurple),
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide(color: Colors.deepPurple),
                                        ),
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: Colors.deepPurple),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color: Colors.deepPurple),
                                        ),
                                      ),
                                      style: TextStyle(color: Colors.deepPurple),


                                    ),
                                  ),
                                )


                              ],
                            ),
                            SizedBox(height: 10),
                            Padding(padding: const EdgeInsets.only(left:10 , right : 10),
                              child: TextFormField(
                                controller: _numClientController,
                                decoration: InputDecoration(
                                  hintText: 'NUMCLIENT',
                                  fillColor: Colors.deepPurple,

                                  hintStyle: TextStyle(color: Colors.deepPurple),
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.deepPurple),
                                  ),
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.deepPurple),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.deepPurple),
                                  ),



                                ),
                                style: TextStyle(color: Colors.deepPurple),
                                validator: (value) {
                                  RegExp regExp = RegExp(r'^[0-9]+$');
                                  if (value == null || value.isEmpty) {
                                    return 'Veuillez entrer le numéro du client';
                                  }
                                  if (!regExp.hasMatch(value)) {
                                    return 'Numéro doit être numérique';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  if (value != null) {
                                    setState(() {
                                      _numclient = int.parse(value);
                                    });
                                  }
                                },
                              ),
                            )

                          ],
                        ),
                      ),

                    ),
                  ],
                ),

SizedBox(height: 20),
                Container(
    padding: EdgeInsets.only(left: 150.0 , bottom: 12.0),
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
    Text(
    'Total Order: ${totalOrder.toStringAsFixed(3)}',
    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.deepPurple),
    ),



    ],
    ),
    ),
    ],
    ),
    ),


    // Part 2: Numeric Keyboard and Action Buttons
                  Container(
                    margin: const EdgeInsets.only(left: 15.0 , right: 15.0),
                    height: 250,
                    child: GridView.count(
                      crossAxisCount: 3,
                      childAspectRatio: 2.5 ,
                      padding: EdgeInsets.all(8.0),
                      mainAxisSpacing: 5.0,
                      crossAxisSpacing: 5.0,
                      children: List.generate(10, (index) {
                        return SizedBox(
                          height: 50,
                          width: 50,
                          child: ElevatedButton(
                            onPressed: () {
                           //   setState(() {
                                _handleDigitPress(index);

                           //   });
                            },
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0),
                              ),
                              padding: EdgeInsets.all(8.0),
                            ),
                            child: Text(
                              '$index',
                              style: TextStyle(fontSize: 16),
                            ),
                          ),


                        );
                      }).toList()
                        ..addAll([
                          SizedBox(
                            height: 50,
                            width: 50,
                            child: ElevatedButton(
                              onPressed: () {
                                resetOrder();
                                                                  },
                              style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(0),
                                ),
                                padding: EdgeInsets.all(8.0),
                              ),
                              child: Text(
                                'Reset',
                                style: TextStyle(fontSize: 16),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 50,
                            width: 50,
                            child: ElevatedButton(
                              onPressed: _updateSelectedProductQuantity,
                              style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(0),
                                ),
                                padding: EdgeInsets.all(8.0),
                              ),
                              child: Text(
                                'VALIDER',
                                style: TextStyle(fontSize: 16),
                              ),
                            ),
                          ),
                        ]),
                    ),

                  ),
                ],
              ),
            ),
          ),

        ],
      ),
    );
  }
}
