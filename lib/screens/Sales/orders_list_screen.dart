import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import '../../models/product.dart';

class OrderListScreen extends StatefulWidget {
  @override
  _OrderListScreenState createState() => _OrderListScreenState();
}

class _OrderListScreenState extends State<OrderListScreen> {
  GraphQLClient setupGraphQLClient() {
    final HttpLink httpLink = HttpLink('http://127.0.0.1:3000/graphql'); // Adjust URL if necessary
    return GraphQLClient(
      link: httpLink,
      cache: GraphQLCache(),
    );
  }

  static const String queryProducts = r'''
  query {
    products {
      id_prod
      name
      description
      price
      image_url
      categorie_id
      quantity_purchased
      stock
      bar_code
      deleted
    }
  }
  ''';

  Future<List<Product>> queryProductsFromGraphQLServer() async {
    final GraphQLClient client = setupGraphQLClient();

    final QueryOptions options = QueryOptions(
      document: gql(queryProducts),
    );

    final QueryResult result = await client.query(options);

    if (result.hasException) {
      throw result.exception!;
    }

    final productsData = result.data!['products'] as List<dynamic>;
    print(productsData);
    return productsData.map((productJson) => Product.fromGraphQLMap(productJson)).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Order List'),
      ),
      body: FutureBuilder<List<Product>>(
        future: queryProductsFromGraphQLServer(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
            return Center(child: Text('No products found.'));
          } else {
            final products = snapshot.data!;

            return ListView.builder(
              itemCount: products.length,
              itemBuilder: (context, index) {
                final product = products[index];

                return ListTile(
                  leading: product.imageUrl != null
                      ? Image.network(product.imageUrl!, width: 50, height: 50)
                      : Icon(Icons.image, size: 50),
                  title: Text(product.name),
                  subtitle: Text('Price: ${product.price}'),
                  trailing: Text('Stock: ${product.stock}'),
                  onTap: () {
                    // Handle product tap
                  },
                );
              },
            );
          }
        },
      ),
    );
  }
}
