import 'package:flutter_crud_app/db/db_helper.dart';
import 'package:flutter_crud_app/db/db_helperProduct.dart';
import '../models/order.dart';
import '../models/product.dart';

class OrderHelper {
  final DBHelper _dbHelper = DBHelper.instance;
  final ProductHelper productHelper = ProductHelper();

 /* Future<void> insertOrder(Order order) async {
    final db = await _dbHelper.database;

    final orderId = await db.insert('orders', order.toMap());

    for (var product in order.products) {
      await db.insert('order_products', {
        'orderId': orderId,
        'productId': product.id,
        'quantity': product.quantityPurchased,
      });
    }
  }
*/

  Future<void> insertOrder(Order order) async {
    final db = await _dbHelper.database;

    final orderId = await db.insert('orders', order.toMap());

    for (var product in order.products) {
      await db.insert('order_products', {
        'orderId': orderId,
        'productId': product.id,
        'quantity': order.productQuantities[product.id],
      });
    }
  }

  Future<List<Order>> fetchAllOrders() async {
    final db = await _dbHelper.database;

    // Fetch all orders from the 'orders' table
    final List<Map<String, dynamic>> orderMaps = await db.query('orders');

    // Convert the list of maps to a list of Order objects
    final List<Order> orders = orderMaps.map((orderMap) {
      return Order(
        id: orderMap['id'] as int?,
        products: [], // Empty list since we're not fetching products
        totalOrder: orderMap['totalOrder'] as double,
        amountPaid: orderMap['amountPaid'] as double?,
        rest: orderMap['rest'] as double?,
        numClient: orderMap['numClient'] as int?,
        date: orderMap['date'] != null ? DateTime.parse(orderMap['date'] as String) : null,
        productQuantities: {}, // Empty map since we're not fetching product quantities
      );
    }).toList();

    return orders;
  }






  Future<Order?> getOrderById(int id) async {
    final db = await _dbHelper.database;

    final orderMap = await db.query('orders', where: 'id = ?', whereArgs: [id]);

    if (orderMap.isNotEmpty) {
      final orderId = orderMap.first['id'] as int;

      final orderProducts = await db.query('order_products', where: 'orderId = ?', whereArgs: [orderId]);
      final List<Product> products = [];

      for (var orderProduct in orderProducts) {
        final productId = orderProduct['productId'] as int;
        final quantity = orderProduct['quantity'] as int;

        final product = await productHelper.getProductById(productId);
        if (product != null) {
          product.quantityPurchased = quantity;
          products.add(product);
        }
      }

      return Order(
        id: orderId,
        products: products,
        totalOrder: orderMap.first['totalOrder'] as double,
        amountPaid: orderMap.first['amountPaid'] as double?,
        rest: orderMap.first['rest'] as double?,
        numClient: orderMap.first['numClient'] as int?,
        date: orderMap.first['date'] != null ? DateTime.parse(orderMap.first['date'] as String) : null,
        productQuantities: _extractProductQuantities(orderMap.first),
      );
    }

    return null;
  }
  Map<int, int> _extractProductQuantities(Map<String, dynamic> map) {
    Map<int, int> productQuantities = {};

    map.forEach((key, value) {
      if (key.startsWith('quantity_')) {
        int productId = int.parse(key.split('_')[1]);
        productQuantities[productId] = value as int;
      }
    });

    return productQuantities;
  }
  Future<int> deleteOrder(int id) async {
    final db = await _dbHelper.database;

    await db.delete('order_products', where: 'orderId = ?', whereArgs: [id]);
    return await db.delete('orders', where: 'id = ?', whereArgs: [id]);
  }

  Future<void> clearOrders() async {
    final db = await _dbHelper.database;

    await db.delete('orders');
  }


  Future<List<Order>> fetchOrdersForToday() async {
    final db = await _dbHelper.database;
    final today = DateTime.now();
    final startOfDay = DateTime(today.year, today.month, today.day);
    final endOfDay = startOfDay.add(Duration(days: 1));

    // Query orders for the current date
    final List<Map<String, dynamic>> orderMaps = await db.query(
      'orders',
      where: 'date BETWEEN ? AND ?',
      whereArgs: [startOfDay.toIso8601String(), endOfDay.toIso8601String()],
    );

    final List<Order> orders = [];

    for (var orderMap in orderMaps) {
      final orderId = orderMap['id'] as int;

      // Fetch the associated products for each order from 'order_products' table
      final orderProducts = await db.query(
        'order_products',
        where: 'orderId = ?',
        whereArgs: [orderId],
      );

      final List<Product> products = [];
      final Map<int, int> productQuantities = {};

      for (var orderProduct in orderProducts) {
        final productId = orderProduct['productId'] as int;
        final quantity = orderProduct['quantity'] as int;

        final product = await productHelper.getProductById(productId);
        if (product != null) {
          products.add(product);
          productQuantities[productId] = quantity;
        }
      }

      orders.add(Order(
        id: orderId,
        products: products,
        totalOrder: orderMap['totalOrder'] as double,
        amountPaid: orderMap['amountPaid'] as double?,
        rest: orderMap['rest'] as double?,
        numClient: orderMap['numClient'] as int?,
        date: orderMap['date'] != null ? DateTime.parse(orderMap['date'] as String) : null,
        productQuantities: productQuantities,
      ));
    }

    return orders;
  }


}
