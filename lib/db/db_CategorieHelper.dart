
import 'package:flutter_crud_app/db/db_helper.dart';
import 'package:flutter_crud_app/models/categorie.dart';
class CategorieHelper {

final DBHelper _dbHelper =DBHelper.instance;


Future<int> insertCategorie(Map<String, dynamic> row) async {
  final db = await _dbHelper.database;
  return await db.insert('categories', row);
}

Future<List<Categorie>> queryAllCategories() async {
  final db = await _dbHelper.database;
  final List<Map<String, dynamic>> maps = await db.query('categories');
  return List.generate(maps.length, (i) {
    return Categorie(
      id: maps[i]['id'],
      nom: maps[i]['nom'],
    );
  });
}



Future<int> updateCategorie(Map<String, dynamic> row) async {
  final db = await _dbHelper.database;
  int id = row['id'];
  return await db.update('categories', row, where: 'id = ?', whereArgs: [id]);
}


Future<int> deleteCategorie(int id) async {
  final db = await _dbHelper.database;
  return await db.delete('categories', where: 'id = ?', whereArgs: [id]);
}


Future<bool> isCategoryExists(String categoryName) async {
  return await _dbHelper.isRecordExists('categories', 'nom', categoryName);
}

Future<Categorie?> getCategorieById(int id) async {
  final db = await _dbHelper.database;
  List<Map<String, dynamic>> maps = await db.query(
    'categories',
    where: 'id = ?',
    whereArgs: [id],
  );
  if (maps.isNotEmpty) {
    return Categorie.fromMap(maps.first);
  } else {
    return null;
  }
}


}
