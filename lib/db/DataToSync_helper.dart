import 'package:flutter_crud_app/db/db_helper.dart';
import '../models/to_sync.dart';

class DatatosyncHelper {
  final DBHelper _dbHelper = DBHelper.instance;

  Future<void> insertDatatosyncProduct(int productId) async {
    final db = await _dbHelper.database;
    await db.insert('datatosync_products', {

      'productId': productId,
    });
  }

  Future<List<DataToSync>> getAllDatatosyncProducts() async {
    final db = await _dbHelper.database;

    // Query the table and get all rows
    final List<Map<String, dynamic>> productMaps = await db.query('datatosync_products');

    // Convert each map into a DataToSync object
    List<DataToSync> dataToSyncList = productMaps.map((map) {
      return DataToSync(
        id: map['id'] as int?,
        products: [map['productId'] as int],
        sync: map['sync'] as int,
      );
    }).toList();

    return dataToSyncList;
  }


  Future<void> clearDataToSyncProducts() async {
    final db = await _dbHelper.database;
    await db.delete('datatosync_products');
  }

  Future<void> updateDataToSyncProduct(int id, int newSyncValue) async {
    final db = await _dbHelper.database;
    await db.update(
      'datatosync_products',
      {'sync': newSyncValue},
      where: 'id = ?',
      whereArgs: [id],
    );
    print('Product sync value updated to $newSyncValue successfully');
  }



}
