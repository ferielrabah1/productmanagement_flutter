import 'package:flutter_crud_app/db/db_helper.dart';
import '../models/product.dart';

class ProductHelper {
  final DBHelper _dbHelper = DBHelper.instance;

  Future<int> insertProduct(Product product) async {
    final db = await _dbHelper.database;
    return await db.insert('products', product.toMap());
  }

  Future<void> clearProducts() async {
    final db = await _dbHelper.database;

    await db.delete('products');
  }

  Future<List<Map<String, dynamic>>> queryAllProducts() async {
    final db = await _dbHelper.database;
    final List<Map<String, dynamic>> maps = await db.query(
      'products',
      where: 'deleted = ?',
      whereArgs: [0],
    );
    return maps;
  }

  Future<int> updateProduct(Product product) async {
    final db = await _dbHelper.database;
    return await db.update(
      'products',
      product.toMap(),
      where: 'id = ?',
      whereArgs: [product.id],
    );
  }

  Future<void> deleteProduct(int id) async {
    final db = await _dbHelper.database;
    await db.update(
      'products',
      {'deleted': 1},
      where: 'id = ?',
      whereArgs: [id],
    );
  }
  Future<void> deleteProductPH(int id) async {
    final db = await _dbHelper.database;
    await db.delete(
      'products',
      where: 'id = ?',
      whereArgs: [id],
    );
  }


  Future<bool> isProductExists(String productName) async {
    return await _dbHelper.isRecordExists("products", "name", productName);
  }

  Future<bool> isCodeExists(String barCode) async {
    return await _dbHelper.isRecordExists("products", "barCode", barCode);
  }

  Future<bool> isRefExists(int ref) async {
    return await _dbHelper.isRecordExists("products", "ref", ref);
  }

  Future<List<Product>> getProductsByCategoryId(int categoryId) async {
    final db = await _dbHelper.database;
    final maps = await db.query(
      'products',
      where: 'categorieId = ?',
      whereArgs: [categoryId],
    );
    return List.generate(maps.length, (i) {
      return Product.fromMap(maps[i]);
    });
  }

  Future<Product?> getProductById(int id) async {
    final db = await _dbHelper.database;
    final maps = await db.query(
      'products',
      where: 'id = ?',
      whereArgs: [id],
    );
    if (maps.isNotEmpty) {
      return Product.fromMap(maps.first);
    } else {
      return null;
    }
  }

  Future<double> getProductPriceById(int productId) async {
    final db = await _dbHelper.database;

    final List<Map<String, dynamic>> maps = await db.query(
      'products',
      columns: ['price'],
      where: 'id = ?',
      whereArgs: [productId],
    );

    if (maps.isNotEmpty) {
      return (maps.first['price'] as num).toDouble();
    } else {
      // Handle the case where the product is not found
      throw Exception('Product not found');
    }
  }


}
