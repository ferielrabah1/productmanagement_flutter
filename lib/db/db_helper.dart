import 'dart:io';

import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:sqflite_common/sqlite_api.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';
class DBHelper {
  static final DBHelper instance = DBHelper._internal();
  static late DatabaseFactory dbFactory;
  Database? _database;

  DBHelper._internal() {
    dbFactory = databaseFactoryFfi;
  }

  Future<Database> get database async {
    if (_database == null || !_database!.isOpen) {
      _database = await openMyDatabase();
    }
    return _database!;
  }

  static final String createDatatosyncTable = '''
    CREATE TABLE datatosync (
      id INTEGER PRIMARY KEY AUTOINCREMENT
       
    )
  ''';

  static final String createDatatosyncTable2 = '''
    CREATE TABLE datatosync_products (
  datatosyncId INTEGER,
  productId INTEGER,
  FOREIGN KEY (datatosyncId) REFERENCES datatosync (id)
)
  ''';

  static final String dropDatatosyncTable = '''
    DROP TABLE IF EXISTS datatosync
  ''';
  Future<Database> openMyDatabase() async {
    final dbPath = await getDatabasesPath();
   final path = join(dbPath, 'prodsGestion_db1.db');
   //final exeDir = File(Platform.resolvedExecutable).parent.path;
   // final path = join(exeDir, 'prodsGestion_db1.db');
    // Assuming the Release directory is the correct one
    return await dbFactory.openDatabase(
      path,
      options: OpenDatabaseOptions(
        version: 38,
        onCreate: (db, version) async {
          await db.execute(
            'CREATE TABLE products(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, description TEXT, price REAL, stock INTEGER,imageUrl TEXT, barCode TEXT,categorieId INTEGER, ref INTEGER,quantityPurchased INTEGER,deleted INTEGER DEFAULT 0, FOREIGN KEY (categorieId) REFERENCES categories(id))',
          );
          await db.execute(
            'CREATE TABLE categories(id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT)',
          );
          await db.execute('''
          CREATE TABLE orders (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            totalOrder REAL,
            amountPaid REAL,
            rest REAL,
            numClient INTEGER,
            date TEXT
          )
        ''');

          await db.execute('''
          CREATE TABLE order_products (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            orderId INTEGER,
            productId INTEGER,
            quantity INTEGER,
            FOREIGN KEY (orderId) REFERENCES orders(id),
            FOREIGN KEY (productId) REFERENCES products(id)
          )
        ''');

          await db.execute(createDatatosyncTable2);
        },

        onUpgrade: (db, oldVersion, newVersion) async {
        /* if (oldVersion < 23) {
          await db.execute('ALTER TABLE products ADD COLUMN deleted INTEGER DEFAULT 0');
          }
*/
          /*if (oldVersion < 8) {
            await db.execute('ALTER TABLE products ADD COLUMN ref INTEGER');
          }*/

          /*if(oldVersion < 9 ) {
            await db.execute('ALTER TABLE orders ADD COLUMN date TEXT');
          }*/
         /* if (oldVersion <10){

                await db.execute('''
      CREATE TABLE datatosync (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        modifiedProducts TEXT
      )
    ''');
*/
/*
         if (oldVersion < 38) {
           await db.execute('ALTER TABLE products ADD COLUMN deleted INTEGER DEFAULT 0');
           await db.execute('ALTER TABLE products ADD COLUMN ref INTEGER');

           await db.execute(createDatatosyncTable2);

       await db.execute('ALTER TABLE orders ADD COLUMN date TEXT');

    print('Upgraded database to version');
    }*/



}


      ),
    );
  }

  Future<bool> isRecordExists(String tableName, String columnName, dynamic value) async {
    final db = await database;
    List<Map<String, dynamic>> result = await db.query(
      tableName,
      where: '$columnName = ?',
      whereArgs: [value],
    );
    return result.isNotEmpty;
  }



  Future<int> getDatabaseVersion() async {
    final db = await database;
    return await db.getVersion();
  }

  Future<bool> isColumnExists(String tableName, String columnName) async {
    final db = await database;
    final result = await db.rawQuery('PRAGMA table_info($tableName)');
    for (var column in result) {
      if (column['name'] == columnName) {
        return true;
      }
    }
    return false;
  }

}






