


import 'dart:async';

import 'package:dio/dio.dart';

class ServerAccessibilityService {
  final String baseUrl;
  late Dio _dio;
  Timer? _timer;


  ServerAccessibilityService({required this.baseUrl}) {
    _dio = Dio(BaseOptions(baseUrl: baseUrl));
  }
// Method to check server accessibility by making a HEAD request
  Future<bool> checkServerAccessibility() async {
    try {
      Response response = await _dio.head('');
      return response.statusCode == 200;
    } catch (e) {
      print('Error checking server accessibility: $e');
      return false;
    }
  }

  // Method to start automatic periodic checking of server accessibility
  void startAutomaticChecking(Duration interval) {
    Timer.periodic(interval, (Timer timer) async {
      bool isAccessible = await checkServerAccessibility();
      if (isAccessible) {
        print('Server is reachable.');
      } else {
        print('Server is not reachable.');
      }
    });
  }
  // Method to stop the automatic periodic checking

  void stopCheckingServerAccessibility() {
     _timer?.cancel();
  }
}
