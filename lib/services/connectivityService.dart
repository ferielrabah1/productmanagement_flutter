
import 'dart:async';
import 'package:flutter_crud_app/services/serverAccessibilityService.dart';
import 'package:internet_connection_checker_plus/internet_connection_checker_plus.dart';

class ConnectivityService {
  final InternetConnection _internetConnection = InternetConnection(); // Instance to check internet connection
  late StreamSubscription<InternetStatus> _subscription; // Subscription to listen for internet status changes
  bool _isConnected = false; // Variable to store the current connectivity status


  // Getter to access the current connectivity status

  bool get isConnected => _isConnected;
// Constructor to initialize the connectivity service and listen for status changes
  ConnectivityService() {
    _subscription = _internetConnection.onStatusChange.listen((InternetStatus status) {
      _isConnected = status == InternetStatus.connected;
    });

    initialize();
  }
  // Method to initialize the connectivity status
  Future<void> initialize() async {
    _isConnected = await _internetConnection.hasInternetAccess;
  }

  void dispose() {
    _subscription.cancel();
  }

  StreamSubscription<InternetStatus> get subscription => _subscription;
}