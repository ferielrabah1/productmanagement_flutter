class Categorie {
  final int? id;
  final String nom;

  Categorie({ this.id, required this.nom});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nom': nom,
    };
  }

  static Categorie fromMap(Map<String, dynamic> map) {
    return Categorie(
      id: map['id'],
      nom: map['nom'],
    );
  }
  static Categorie fromJson(Map<String, dynamic> json) {
    return Categorie(
        id: json['id'],
        nom: json['nom'],


    );
  }
}
