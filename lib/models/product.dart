
import 'package:flutter/material.dart';

import 'categorie.dart';

class Product {
  final int? id;
    String name;
  final String description;
  late final double price;
   String imageUrl;
  final int categorieId;
  int quantityPurchased;
  int stock;
  final int? ref ;
  final String barCode;
  bool deleted;


  Product({this.id, required this.name,
    required this.description,
    required this.price,
    required this.imageUrl,
     required this.categorieId,
    this.quantityPurchased=0,
    required this.ref,
    required this.stock,
    required this.barCode,
    this.deleted = false
  });
  double get totalPrice => price * quantityPurchased;
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'description': description,
      'price': price,
      'imageUrl': imageUrl,
      'categorieId' : categorieId,
      'quantityPurchased': quantityPurchased,
      'stock' : stock,
      'ref' : ref,
      'barCode' : barCode,
      'deleted': deleted ? 1 : 0,
    };
  }
  static Product fromMap(Map<String, dynamic> map) {
    return Product(
      id: map['id'] as int?,
      name: map['name'] as String? ?? '',
      description: map['description'] as String? ?? '',
      price: (map['price'] as num).toDouble(),
      imageUrl: map['imageUrl'] as String? ?? '',
      categorieId: map['categorieId'] as int,
      quantityPurchased: map['quantityPurchased'] as int? ?? 0,
      stock: map['stock'] as int? ?? 0,
      ref: map['ref'] as int?,
      barCode: map['barCode'] as String? ?? '',
      deleted: map['deleted'] == 1,
    );
  }

  static Product fromGraphQLMap(Map<String, dynamic> map) {
    return Product(
      id: map['id_prod'] as int?,
      name: map['name'] as String? ?? '',
      description: map['description'] as String? ?? '',
      price: (map['price'] as num).toDouble(),
      imageUrl: map['image_url'] as String? ?? '',
      categorieId: map['categorie_id'] as int,
      quantityPurchased: map['quantity_purchased'] as int? ?? 0,
      stock: map['stock'] as int? ?? 0,
      barCode: map['bar_code'] as String? ?? '',
      deleted: map['deleted'] == 1,
        ref: map['ref'] as int?,

    );
  }

  static Product fromJson(Map<String, dynamic> json) {
    return Product(
      id: json['id'] as int?,
      name: json['name'] as String? ?? '',
      description: json['description'] as String? ?? '',
      price: (json['price'] as num).toDouble(),
      imageUrl: json['imageUrl'] as String? ?? '',
      categorieId: json['categorieId'] as int,
      quantityPurchased: json['quantityPurchased'] as int? ?? 0,
      stock: json['stock'] as int? ?? 0,
      ref: json['ref'] as int?,
      barCode: json['barCode'] as String? ?? '',
      deleted: json['deleted'] == 1,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'description': description,
      'price': price,
      'stock': stock,
      'imageUrl': imageUrl,
      'barCode': barCode,
      'categorieId': categorieId,
      'quantityPurchased': quantityPurchased,
      'deleted': deleted ? 1 : 0,
    };
  }




}
