import 'dart:convert';

class DataToSync {
  final int? id;
  final List<int> products;
  int sync;

  DataToSync({
    this.id,
    required this.products,
    this.sync = 0

});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'productIds': jsonEncode(products),
      'sync': sync,
    };
  }

  static DataToSync fromMap(Map<String, dynamic> map, List<int> productIds) {
    return DataToSync(
      id: map['id'],
      products: productIds,
      sync: map['sync']
    );
  }
}
