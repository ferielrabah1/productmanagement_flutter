import 'package:flutter_crud_app/models/product.dart';

class Order {
  final int? id;
  final List<Product> products;
  final double totalOrder;
  final double? amountPaid;
  final double? rest;
  final int? numClient;
  final DateTime? date;
  final Map<int, int> productQuantities;

  Order({this.id,
    required this.products,
    required this.totalOrder,
     this.amountPaid,
    this.rest,
     this.numClient,
     this.date,
    required this.productQuantities,

  });

  double get total {
    return products.fold(0, (sum, product) => sum + product.totalPrice);
  }


  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'totalOrder': totalOrder,
      'amountPaid' : amountPaid,
      'rest' : rest,
      'numClient' : numClient,
      'date' : date?.toIso8601String()
    };
  }

  static Future<Order> fromMap(Map<String, dynamic> map, Future<List<Product>> Function(List<int>) getProductByIds) async {
    Map<int, int> productQuantities = {};
    List<int> productIds = [];

    map.forEach((key, value) {
      if (key.startsWith('quantity_')) {
        int productId = int.parse(key.split('_')[1]);
        productQuantities[productId] = value;
        productIds.add(productId);
      }
    });

    // Fetch the products using the product IDs
    List<Product> products = await getProductByIds(productIds);

    return Order(
      id: map['id'],
      products: products,
      totalOrder: map['totalOrder'],
      amountPaid: map['amountPaid'],
      rest: map['rest'],
      numClient: map['numClient'],
      date: map['date'] != null ? DateTime.parse(map['date']) : null,
      productQuantities: productQuantities,
    );
  }
}
