class ProductsModified {
  final int id;
  final int productId;
  final String typeOperation;
  final DateTime timeOperation;
  bool sync;

  ProductsModified({
    required this.id,
    required this.productId,
    required this.typeOperation,
    required this.timeOperation,
    this.sync = false,
  });

  factory ProductsModified.fromMap(Map<String, dynamic> map) {
    return ProductsModified(
      id: map['id'],
      productId: map['productId'],
      typeOperation: map['type_operation'],
      timeOperation: DateTime.parse(map['time_operation']),
      sync: map['sync'] ?? false,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'productId': productId,
      'type_operation': typeOperation,
      'time_operation': timeOperation.toIso8601String(),
      'sync': sync,
    };
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'productId': productId,
      'type_operation': typeOperation,
      'time_operation': timeOperation.toIso8601String(),
      'sync': sync,
    };
  }
}