
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_crud_app/screens/Sales/orders_list_screen.dart';

import 'package:flutter_crud_app/screens/Sales/sales_management.dart';

import 'package:sqflite/sqflite.dart';

import 'package:sqflite_common_ffi/sqflite_ffi.dart';
import 'dart:ffi';
import 'dart:io';
import 'package:sqlite3/open.dart';
import 'package:sqlite3/sqlite3.dart';
DynamicLibrary _openSqliteUnderWindows() {
  // Get the directory of the executable
  final exeDir = File(Platform.resolvedExecutable).parent;
  //print(exeDir);

  // Assuming the Release directory is the correct one
  final expectedDir = exeDir.path.replaceAll(r'\Debug', r'\Release');
  final libraryNextToExe = File('$expectedDir/sqlite3.dll');
 // print(expectedDir);

  if (libraryNextToExe.existsSync()) {
    return DynamicLibrary.open(libraryNextToExe.path);
  } else {
    // Optionally, throw an error or handle the case where the DLL is not found
    throw Exception('sqlite3.dll not found in the expected location: ${libraryNextToExe.path}');
  }
}
Future<void> main() async {
  if ((defaultTargetPlatform == TargetPlatform.windows || defaultTargetPlatform == TargetPlatform.linux) && !kIsWeb) {
    // Initialize FFI to be able to use database with Windows/Linux
    // We use 'sqflite_ffi' if Linux or Windows, else we can use default sqlite (Android/IOS/Mac)
    if (defaultTargetPlatform == TargetPlatform.linux) {
      sqfliteFfiInit();
    } else if (defaultTargetPlatform == TargetPlatform.windows) {
      open.overrideFor(OperatingSystem.windows, _openSqliteUnderWindows);
      final db = sqlite3.openInMemory();
      db.dispose();
    }

    // Change the default factory
    databaseFactory = databaseFactoryFfi;
  }

  
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Product Management',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SalesManagementScreen(),
    );
  }
}
